package sogeti.cir.fsn.model;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import sogeti.cir.fsn.model.neo4j.NeoModel;

@NodeEntity
@Component
public class Role extends NeoModel {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "role";

	@NotNull
	@NotEmpty
	private String roleName;
	
	@JsonIgnore
	@Relationship(type = Utilisateur.HAS_A_ROLE, direction = Relationship.INCOMING)
	private Set<Utilisateur> utilisateurs;

	public String getName() {
		return NAME;
	}

	public String getRoleName() {
		return roleName;
	}

	public Set<Utilisateur> getUtilisateurs() {
		if (utilisateurs == null)
			utilisateurs = new HashSet<>();
		return utilisateurs;
	}

	public void setRoleName(String nom) {
		this.roleName = nom;
	}

	public void setUtilisateurs(Set<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	@Override
	public String getLabel() {
		return roleName;
	}
	
}