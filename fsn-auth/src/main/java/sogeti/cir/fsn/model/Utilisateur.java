package sogeti.cir.fsn.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import sogeti.cir.fsn.model.neo4j.NeoModel;

@NodeEntity
@Component
public class Utilisateur extends NeoModel {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "utilisateur";
	public static final String HAS_A_ROLE = "A Un Role";
	private static final String TO_WORK_ON = "A TRAVAILLE SUR";

	@NotNull
	@NotEmpty
	private String userName;

	@NotNull
	@NotEmpty
	private String password;
	private String password_bis;
	private boolean enabled;

	@Relationship(type = HAS_A_ROLE)
	private Set<Role> roles;
	
	@JsonIgnore
	@Relationship(type = TO_WORK_ON)
	private Chantier chantier;

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getPassword_bis() {
		return password_bis;
	}
	
	public Set<Role> getRoles() {
		if (roles == null)
			roles = new HashSet<>();
		return roles;
	}

	public void setUserName(String identifiant) {
		this.userName = identifiant;
	}

	public void setPassword(String motDePasse) {
		this.password = motDePasse;
	}

	public void setPassword_bis(String motDePasse2) {
		this.password_bis = motDePasse2;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String getLabel() {
		return userName;
	}

	@Override
	public String getName() {
		return NAME;
	}

	public Chantier getChantier() {
		return chantier;
	}

	public void setChantier(Chantier chantier) {
		this.chantier = chantier;
	}
	public Utilisateur() {};

	public Utilisateur(String userName, String password, String password_bis, boolean enabled) {
		super();
		this.userName = userName;
		this.password = password;
		this.password_bis = password_bis;
		this.enabled = enabled;
	}
	
	

}