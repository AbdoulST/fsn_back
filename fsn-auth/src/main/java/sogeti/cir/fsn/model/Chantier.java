package sogeti.cir.fsn.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.neo4j.ogm.annotation.NodeEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import sogeti.cir.fsn.model.neo4j.NeoModel;

@Component
@NodeEntity
public class Chantier extends NeoModel {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "chantier";
	public static final String EST_CONCERNER_PAR = "EstConcernerPar";
	public static final String MON_CHANTIER = "monChantier";

	private String numero;

	private String nomAbrege;

	private String nom;


	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNomAbrege() {
		return nomAbrege;
	}

	public void setNomAbrege(String nomAbrege) {
		this.nomAbrege = nomAbrege;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String getLabel() {
		return NAME;
	}

}
