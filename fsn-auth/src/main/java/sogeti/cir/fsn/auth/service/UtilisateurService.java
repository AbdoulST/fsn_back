package sogeti.cir.fsn.auth.service;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.auth.dao.RoleDAO;
import sogeti.cir.fsn.auth.dao.UtilisateurDAO;
import sogeti.cir.fsn.model.Role;
import sogeti.cir.fsn.model.Utilisateur;

@Service
public class UtilisateurService extends AbstractService<Utilisateur> {

	@Autowired
	protected RoleDAO roleDao;
	
	//@Autowired
	//protected UtilisateurDAO utilisateurDAO;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UtilisateurService(AbstractDao<Utilisateur> dao) {
		super(dao);
	}

	public void inscription(@Valid Utilisateur user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setPassword_bis(user.getPassword());
		user.setRoles((Set<Role>)(roleDao.findAll())); 
		dao.save(user);
	}
	
	
	@Transactional
	public Utilisateur createUser(@Valid String userName,@Valid String password, Role...roles) {
		Utilisateur user = new Utilisateur();
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setPassword_bis(user.getPassword());
		for (Role r : roles) {
			user.getRoles().add(r);
		}
		//dao.save(user);
		return dao.save(user);
	}
	

	public Utilisateur listeUtilisateur(String userName) {
		return ((UtilisateurDAO) dao).findByUserName(userName);
	}
}