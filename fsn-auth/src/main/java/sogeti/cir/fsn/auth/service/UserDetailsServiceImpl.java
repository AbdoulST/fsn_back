package sogeti.cir.fsn.auth.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sogeti.cir.fsn.auth.dao.UtilisateurDAO;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UtilisateurDAO utilisateurDao;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (username == null)
			throw new UsernameNotFoundException("utilisateur inconnu");
		return new MyUserDetails(utilisateurDao.findByUserName(username));
	}
	

}