package sogeti.cir.fsn.auth.validator;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import sogeti.cir.fsn.model.Utilisateur;


@Component(value="beforeCreateUtilisateurValidator")
public class UtilisateurValidator implements Validator {
	

	@Override
	public boolean supports(Class<?> aClass) {
		return Utilisateur.class.equals(aClass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Utilisateur user = (Utilisateur) target;
		String name = user.getUserName();
		
		if (name == null || name.length() < 3 || name.length() > 50) {
			errors.rejectValue("username", "name is empty or too short or too long");
		}
			
		if (!user.getPassword_bis().equals(user.getPassword())) {
			errors.rejectValue("password_bis", "Mot de passe non identique !");
		}
	}
}