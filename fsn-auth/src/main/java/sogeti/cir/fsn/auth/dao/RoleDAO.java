package sogeti.cir.fsn.auth.dao;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.Role;

@RepositoryRestResource(collectionResourceRel = Role.NAME, path = Role.NAME)
public interface RoleDAO extends AbstractDao<Role> {
	
	Role findByRoleName(String nom);
	Role createRole(String roleName);
}