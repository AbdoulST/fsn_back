package sogeti.cir.fsn.auth.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.model.Role;
import sogeti.cir.fsn.model.Utilisateur;

@Service
public class DatabaseService implements CommandLineRunner {

	private static final String ROLE_ADMIN = "Administrateur";
	private static final String USER_IDENTIFIANT_ADMIN = "root";
	private static final String USER_PASSWORD_ADMIN = "root";

	protected final Logger log = LoggerFactory.getLogger(getClass());

	protected UtilisateurService utilisateurDAO;
	protected RoleService roleDAO;
	
	@Autowired
	private PasswordEncoder myPasswordEncoder;

	public DatabaseService(UtilisateurService utilisateurDAO, RoleService roleDAO) {
		this.utilisateurDAO = utilisateurDAO;
		this.roleDAO = roleDAO;
	}

	public void run(String[] args) throws Exception {
		gestionUtilisateursEtRole();
	}

	/*private void gestionUtilisateursEtRole() {
		long nbRoles = roleDAO.count();
		long nbUtilisateurs = utilisateurDAO.count();
		if (nbRoles == 0) {
			Role role = new Role();
			role.setNom(ROLE_ADMIN);
			try {
				roleDAO.save(role);
			} catch (NotValidObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ResourceNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (nbUtilisateurs == 0) {
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdentifiant(USER_IDENTIFIANT_ADMIN);
			utilisateur.setMotDePasse(USER_PASSWORD_ADMIN);
			utilisateur.setMotDePasse2(utilisateur.getMotDePasse());
			utilisateurDAO.inscription(utilisateur);
		}
	}*/
	
	private void gestionUtilisateursEtRole() {
		long nbRoles = roleDAO.count();
		long nbUtilisateurs = utilisateurDAO.count();
		
		if (nbUtilisateurs == 0) {
			
			log.info("base seem empty, initialising default content...");
			Role r_admin = roleDAO.createRole("ROLE_ADMIN");
			Role r_user = roleDAO.createRole("ROLE_USER");
			Role r_visitor = roleDAO.createRole("ROLE_VISITOR");
			

			Utilisateur u_admin = utilisateurDAO.createUser("admin",
											myPasswordEncoder.encode("admin"),
											 r_admin, r_user);
			Utilisateur u_maurille = utilisateurDAO.createUser("maurille",
					myPasswordEncoder.encode("1234"), r_user);
			Utilisateur u_user = utilisateurDAO.createUser("user",
					myPasswordEncoder.encode("root"), r_visitor);
			
		}
		else
			log.info("base isn't empty, no initialisation required");
		
	}
}