package sogeti.cir.fsn.auth.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import sogeti.cir.fsn.model.Utilisateur;

public class MyUserDetails implements UserDetails {
	
	private Utilisateur user;
	public MyUserDetails(Utilisateur user) {
		this.user = user;
	}
	
	@Override
	public String getPassword() { return this.user.getPassword();}
	@Override
	public String getUsername() { return this.user.getUserName();}
	@Override
	public boolean isEnabled() {return this.user.isEnabled();}
	@Override
	public boolean isAccountNonExpired() { return true;}
	@Override
	public boolean isAccountNonLocked() { return true;}
	@Override
	public boolean isCredentialsNonExpired() { return true;}

	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		List<GrantedAuthority> grantedAuthorities = null;
		if (user != null && user.getRoles() != null)
			grantedAuthorities = user.getRoles().stream()
									.map(r -> r.getRoleName()) // attention, suppose que les roles sont de forme ROLE_...
									.map(rolename -> new SimpleGrantedAuthority(rolename))
									.collect(Collectors.toList());
		else
			grantedAuthorities = Lists.newArrayList();
		return grantedAuthorities;
	}
	
	public Map<String, Object> getChantier() {
		Map<String, Object> map = Maps.newHashMap();
		map.put("id", user.getChantier()!=null ? user.getChantier().getId() : null);
		map.put("label", user.getChantier()!=null ? user.getChantier().getLabel() : null);
		return map;
	}

	

}
