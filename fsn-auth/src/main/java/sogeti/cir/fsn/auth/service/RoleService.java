package sogeti.cir.fsn.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.auth.dao.RoleDAO;
import sogeti.cir.fsn.model.Role;


@Service
public class RoleService extends AbstractService<Role> {

	public RoleService(AbstractDao<Role> dao) {
		super(dao);
	}
	@Autowired
	private  RoleDAO roleDAO;
	
	@Transactional
	public Role createRole(String role){
		Role r = new Role();
		r.setRoleName(role);  
		return roleDAO.save(r);
	}
	
}