package sogeti.cir.fsn.auth.dao;

import org.springframework.data.geo.GeoResults;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.Role;
import sogeti.cir.fsn.model.Utilisateur;
@RepositoryRestResource(collectionResourceRel = Utilisateur.NAME, path = Utilisateur.NAME)
public interface UtilisateurDAO extends AbstractDao<Utilisateur> {

	Utilisateur findByUserName(String userName);

	@Query("MATCH (u:Utilisateur) RETURN u")
	GeoResults<Utilisateur> tousLesUtilisateurs();
	
	Utilisateur createUser(String userName, String password, Role... roles);
}