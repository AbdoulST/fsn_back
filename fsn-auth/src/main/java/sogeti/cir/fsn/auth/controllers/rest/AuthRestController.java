package sogeti.cir.fsn.auth.controllers.rest;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.auth.service.SecurityService;
import sogeti.cir.fsn.auth.service.UtilisateurService;
import sogeti.cir.fsn.auth.validator.UtilisateurValidator;
import sogeti.cir.fsn.model.Utilisateur;

@RestController
@RequestMapping
public class AuthRestController {
	
	@Autowired
	UtilisateurValidator utilisateurValidator;
	
	@Autowired
	UtilisateurService utilisateurService;
	
	@Autowired
	SecurityService securityService;
	
	@PostMapping("/sign-in")
	public Utilisateur inscription(@Valid Utilisateur utilisateur, BindingResult bindingResult, Model model) {
		utilisateurValidator.validate(utilisateur, bindingResult);
		if (bindingResult.hasErrors()) {
			return null;
		}
		utilisateurService.inscription(utilisateur);
		securityService.autologin(utilisateur.getUserName(), utilisateur.getPassword_bis());
		return utilisateur;
	}
	
	@RequestMapping("/user")
	public Principal me(Principal principal) {
		return principal;
	}
}