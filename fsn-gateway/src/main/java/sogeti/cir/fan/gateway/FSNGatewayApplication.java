package sogeti.cir.fan.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCaching
@EnableZuulProxy
public class FSNGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(FSNGatewayApplication.class, args);
	}
	
}