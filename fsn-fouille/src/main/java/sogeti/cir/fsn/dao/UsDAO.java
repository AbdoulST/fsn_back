package sogeti.cir.fsn.dao;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.query.Param;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.ElementRecueilli;
import sogeti.cir.fsn.model.UniteStratigraphique;

public interface UsDAO extends AbstractDao<UniteStratigraphique> {

	@Query("MATCH (n:"+UniteStratigraphique.LABEL+")--(c:Chantier) WHERE  n.identification CONTAINS {search} AND id(c)= {chantierId} RETURN n LIMIT {limit}")
	public List<UniteStratigraphique> search(@Param("search") String search, @Param("chantierId") long id, @Param("limit") int limit);
	
	@Query("MATCH (n:"+UniteStratigraphique.LABEL+")--(e:"+ElementRecueilli.LABEL+") WHERE id(n) = {id} RETURN count(e) >0")
	public boolean hasER(@Param("id") long id);
	
	public List<UniteStratigraphique>  findByChantierNomAbrege(String numero);
	
	public UniteStratigraphique findFirstByIdentification(String identification);
}
