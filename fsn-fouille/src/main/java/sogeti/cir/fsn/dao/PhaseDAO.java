package sogeti.cir.fsn.dao;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.Phase;

public interface PhaseDAO extends AbstractDao<Phase> {

}
