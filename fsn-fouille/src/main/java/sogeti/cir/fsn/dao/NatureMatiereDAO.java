package sogeti.cir.fsn.dao;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.NatureMatiere;

public interface NatureMatiereDAO extends AbstractDao<NatureMatiere> {

}
