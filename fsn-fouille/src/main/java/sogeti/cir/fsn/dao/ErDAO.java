package sogeti.cir.fsn.dao;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.ElementRecueilli;

public interface ErDAO extends AbstractDao<ElementRecueilli> {
	
	public ElementRecueilli findFirstByIdentification(String identification);

}
