package sogeti.cir.fsn.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.model.Phase;

@RestController
@RequestMapping("/phases")

public class PhaseRestController extends AbstractRestController<Phase> {
	
	public PhaseRestController(AbstractService<Phase> service) {
		super(service);
	}


}
