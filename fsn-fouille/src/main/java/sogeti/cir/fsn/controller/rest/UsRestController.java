package sogeti.cir.fsn.controller.rest;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.model.UniteStratigraphique;
import sogeti.cir.fsn.service.UsService;

@RestController
@RequestMapping("/us")
public class UsRestController extends AbstractRestController<UniteStratigraphique> {

	private UsService usService;
	
	public UsRestController(UsService service) {
		super(service);
		this.usService = service;
	}
	
	@GetMapping("/search-identification")
	public ResponseEntity search(@RequestParam(required = true) Long chantierId, 
								@RequestParam(required = true) String containString,
								@RequestParam(required = false, defaultValue = "20") int limit) {
	 
		try {
			List<UniteStratigraphique> p = this.usService.
					search(chantierId, containString, limit);
			return new ResponseEntity<List<UniteStratigraphique>>(p, HttpStatus.OK);
		}
		catch(IllegalArgumentException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Affiche les USs  appartenant au meme chantier
	 * @param id
	 * @return Liste de US
	 */
	@GetMapping("/chantier/{numChantier}")
    public Iterable<UniteStratigraphique> findAllByChantier(@PathVariable(required = true) String numChantier) {
			return usService.findByChantier(numChantier);
    }
	
	
}
