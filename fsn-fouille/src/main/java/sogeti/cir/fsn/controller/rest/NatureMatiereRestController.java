package sogeti.cir.fsn.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.model.NatureMatiere;

@RestController
@RequestMapping("/nms")
public class NatureMatiereRestController extends AbstractRestController<NatureMatiere> {

	public NatureMatiereRestController(AbstractService<NatureMatiere> service) {
		super(service);
	}
}