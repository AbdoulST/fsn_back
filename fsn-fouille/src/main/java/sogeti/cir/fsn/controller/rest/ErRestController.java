package sogeti.cir.fsn.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.model.ElementRecueilli;


@RestController
@RequestMapping("/ers")
public class ErRestController extends AbstractRestController<ElementRecueilli> {

	public ErRestController(AbstractService<ElementRecueilli> service) {
		super(service);
	}
}