package sogeti.cir.fsn.model.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import sogeti.cir.fsn.model.UniteStratigraphique;

public class UsRelationDeserializer extends StdDeserializer<UniteStratigraphique>{
	
	public UsRelationDeserializer(Class<?> vc) { 
        super(vc); 
    }
	
	public UsRelationDeserializer() { 
        super((Class<?>)null); 
    }

	@Override
	public UniteStratigraphique deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		JsonNode jn = p.getCodec().readTree(p);
		UniteStratigraphique us = new UniteStratigraphique();
		if(jn.has("id"))
			us.setId(jn.get("id").asLong());
		return us;
	}
}
