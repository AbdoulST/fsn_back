package sogeti.cir.fsn.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import sogeti.cir.fsn.model.neo4j.NeoModel;

public class Phase extends NeoModel {
	private static final long serialVersionUID = 1L;

	public static final String NAME = "phase";
	
	@NotNull
	@NotBlank
	private String nom;
	
	@NotNull
	@NotBlank
	private String description;
	
	private int debutFourchetteBasse;
	
	private int debutFourchetteHaute;
	
	private int finFourchetteBasse;
	
	private int finFourchetteHaute;
	

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getDebutFourchetteBasse() {
		return debutFourchetteBasse;
	}


	public void setDebutFourchetteBasse(int debutFourchetteBasse) {
		this.debutFourchetteBasse = debutFourchetteBasse;
	}


	public int getDebutFourchetteHaute() {
		return debutFourchetteHaute;
	}


	public void setDebutFourchetteHaute(int debutFourchetteHaute) {
		this.debutFourchetteHaute = debutFourchetteHaute;
	}


	public int getFinFourchetteBasse() {
		return finFourchetteBasse;
	}


	public void setFinFourchetteBasse(int finFourchetteBasse) {
		this.finFourchetteBasse = finFourchetteBasse;
	}


	public int getFinFourchetteHaute() {
		return finFourchetteHaute;
	}


	public void setFinFourchetteHaute(int finFourchetteHaute) {
		this.finFourchetteHaute = finFourchetteHaute;
	}

	@Override
	public String getLabel() {
		return NAME;
	}

}
