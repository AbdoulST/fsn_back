package sogeti.cir.fsn.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import sogeti.cir.fsn.model.neo4j.NeoModel;
import sogeti.cir.fsn.model.serializer.UsListRelationSerializer;


@Component
@NodeEntity(label = UniteStratigraphique.LABEL)
public class UniteStratigraphique extends NeoModel {

	private static final long serialVersionUID = 1L;
	public static final String NAME = "us";
	public static final String LABEL = "US";
	public static final String IS_SYNCHRONOUS = "EST_SYNCHRONE";
	public static final String IS_PREVIOUS = "EST_ANTERIEURE";
	public static final String IS_LOCATED_ON = "EST_SITUEE_SUR";
	public static final String HAS_A_PHASE = "EST_ASSOCIE_A";
	

	@NotNull
	@Index(unique=true) 
	@Pattern(regexp="^.*\\.[0-9]{1,4}$")
	private String identification;
	
	@Length(min = 0, max = 500)
	private String description;
	
	@Length(min = 0, max = 500)
	private String comment;//commentaire
	
	@Relationship(type = HAS_A_PHASE,  direction = Relationship.OUTGOING )
	private Phase phase;
	
	@NotNull
	@Relationship(type = IS_LOCATED_ON, direction = Relationship.OUTGOING)
	private Chantier chantier;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy", iso = ISO.DATE)
	private Date beginningDate;

	@DateTimeFormat(pattern = "dd/MM/yyyy", iso = ISO.DATE)
	private Date endDate;
	
	
	private String type;
	
	@Length(min = 0, max = 4)
	private String longueur;
	
	@Length(min = 0, max = 4)
	private String largeur;
	
	private String coordonneeLambertX;
	
	private String coordonneeLambertY;
	
	@JsonSerialize(using = UsListRelationSerializer.class)
	@Relationship(type = IS_SYNCHRONOUS, direction = Relationship.UNDIRECTED)
	private Set<UniteStratigraphique> usSynchrones;

	@JsonSerialize(using = UsListRelationSerializer.class)
	@Relationship(type = IS_PREVIOUS, direction = Relationship.INCOMING)
	private Set<UniteStratigraphique> usAnterieures;

	@JsonSerialize(using = UsListRelationSerializer.class)
	@Relationship(type = IS_PREVIOUS, direction = Relationship.OUTGOING)
	private Set<UniteStratigraphique> usPosterieures;
	
	@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
	@Relationship(type = ElementRecueilli.IS_COLLECTED_ON, direction = Relationship.INCOMING)
	private Set<ElementRecueilli> ers;
	
	public Phase getPhase() {
		return phase;
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	
	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Chantier getChantier() {
		return chantier;
	}

	public void setChantier(Chantier chantier) {
		this.chantier = chantier;
	}

	public Date getBeginningDate() {
		return beginningDate;
	}

	public void setBeginningDate(Date beginningDate) {
		this.beginningDate = beginningDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Set<UniteStratigraphique> getUsSynchrones() {
		if (usSynchrones == null)
			usSynchrones = new HashSet<>();
		return usSynchrones;
	}

	public void setUsSynchrones(Set<UniteStratigraphique> usSynchrones) {
		this.usSynchrones = usSynchrones;
	}

	public Set<UniteStratigraphique> getUsAnterieures() {
		if (usAnterieures == null)
			usAnterieures = new HashSet<>();
		return usAnterieures;
	}

	public void setUsAnterieures(Set<UniteStratigraphique> usAnterieures) {
		this.usAnterieures = usAnterieures;
	}

	public Set<UniteStratigraphique> getUsPosterieures() {
		if (usPosterieures == null)
			usPosterieures = new HashSet<>();
		return usPosterieures;
	}

	public void setUsPosterieures(Set<UniteStratigraphique> usPosterieures) {
		this.usPosterieures = usPosterieures;
	}

	public void setErs(Set<ElementRecueilli> ers) {
		this.ers = ers;
	}

	public Set<ElementRecueilli> getErs() {
		if (ers == null)
			ers = new HashSet<>();
		return ers;
	}
	
	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return UniteStratigraphique.LABEL;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return NAME;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLongueur() {
		return longueur;
	}

	public void setLongueur(String longueur) {
		this.longueur = longueur;
	}

	public String getLargeur() {
		return largeur;
	}

	public void setLargeur(String largeur) {
		this.largeur = largeur;
	}

	public String getCoordonneeLambertX() {
		return coordonneeLambertX;
	}

	public void setCoordonneeLambertX(String coordonneeLambertX) {
		this.coordonneeLambertX = coordonneeLambertX;
	}

	public String getCoordonneeLambertY() {
		return coordonneeLambertY;
	}

	public void setCoordonneeLambertY(String coordonneeLambertY) {
		this.coordonneeLambertY = coordonneeLambertY;
	}

}
