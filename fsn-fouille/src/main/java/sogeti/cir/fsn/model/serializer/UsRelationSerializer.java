package sogeti.cir.fsn.model.serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import sogeti.cir.fsn.model.UniteStratigraphique;

public class UsRelationSerializer  extends StdSerializer<UniteStratigraphique> {

	protected UsRelationSerializer(Class<UniteStratigraphique> t) {
		super(t);
	}

	protected UsRelationSerializer() {
		this(null);
	}

	@Override
	public void serialize(UniteStratigraphique us, JsonGenerator jg, SerializerProvider sp)
			throws IOException {
		jg.writeObject(new SimpleUsRelation(us));
	}

}
