package sogeti.cir.fsn.model.serializer;

import sogeti.cir.fsn.model.UniteStratigraphique;

public class SimpleUsRelation {
	
	private Long id;
	
	private String identification;
	
	public SimpleUsRelation(UniteStratigraphique us) {
		this.id = us.getId();
		this.identification = us.getIdentification();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}
	
	

}
