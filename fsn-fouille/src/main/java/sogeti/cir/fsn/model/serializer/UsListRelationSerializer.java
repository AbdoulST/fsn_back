package sogeti.cir.fsn.model.serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import sogeti.cir.fsn.model.UniteStratigraphique;

public class UsListRelationSerializer  extends StdSerializer<Set<UniteStratigraphique>> {

	protected UsListRelationSerializer(Class<Set<UniteStratigraphique>> t) {
		super(t);
	}

	protected UsListRelationSerializer() {
		this(null);
	}

	@Override
	public void serialize(Set<UniteStratigraphique> set, JsonGenerator jg, SerializerProvider sp)
			throws IOException {
		List<SimpleUsRelation> ids = new ArrayList<>();
		for (UniteStratigraphique u : set) {
			ids.add(new SimpleUsRelation(u));
		}
		jg.writeObject(ids);
	}

}
