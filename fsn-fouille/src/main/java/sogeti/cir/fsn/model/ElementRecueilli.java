package sogeti.cir.fsn.model;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonFormat;

import sogeti.cir.fsn.model.deserializer.UsRelationDeserializer;
import sogeti.cir.fsn.model.neo4j.NeoModel;
import sogeti.cir.fsn.model.serializer.UsRelationSerializer;

import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.neo4j.ogm.annotation.Relationship;

@Component
@NodeEntity(label=ElementRecueilli.LABEL)
public class ElementRecueilli extends NeoModel {
	
	private static final long serialVersionUID = 1L;

	public static final String LABEL = "ER";
	public static final String NAME = LABEL.toLowerCase();
	public static final String IS_COLLECTED_ON = "EST_RECUEILLI_SUR";
	
	@NotNull
	@Index(unique=true) 
	@Pattern(regexp="^.*\\.[0-9]{1,4}$")
	private String identification;
	
	@NotNull
	@Size(min=5, max=255)
	private String description;
	
	@NotNull
	private Date dateDecouverte;
	
	
	@JsonSerialize(using = UsRelationSerializer.class)
	@JsonDeserialize(using = UsRelationDeserializer.class)
	@Relationship(type = IS_COLLECTED_ON, direction = Relationship.OUTGOING )
	private UniteStratigraphique uniteStratigraphique;
	
	@Relationship(type = "HAS", direction = Relationship.OUTGOING)
	private NatureMatiere natureMatiere; 
	
	public ElementRecueilli() {
		super();
	}

	public String getIdentification() {
		return identification;
	}


	public void setIdentification(String identification) {
		this.identification = identification;
	}


	public String getDescription() {
		return description;
	}

	
	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateDecouverte() {
		return dateDecouverte;
	}

	public void setDateDecouverte(Date dateDecouverte) {
		this.dateDecouverte = dateDecouverte;
	}

	public UniteStratigraphique getUniteStratigraphique() {
		return uniteStratigraphique;
	}
	
	public void setUniteStratigraphique(UniteStratigraphique uniteStratigraphique) {
		this.uniteStratigraphique = uniteStratigraphique;
	} 

	public NatureMatiere getNatureMatiere() {
		return natureMatiere;
	}

	public void setNatureMatiere(NatureMatiere natureMatiere) {
		this.natureMatiere = natureMatiere;
	}
	
	@Override
	public String getLabel() {
		return LABEL;
	}

	@Override
	public String getName() {
		return NAME;
	}
}