package sogeti.cir.fsn.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.neo4j.ogm.annotation.NodeEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import sogeti.cir.fsn.model.neo4j.NeoModel;

@Component
@NodeEntity
public class Chantier extends NeoModel {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "chantier";

	@NotNull
	@NotBlank
	@Length(min = 11, max = 11)
	private String numero;

	@NotNull
	@NotBlank
	@Length(min = 3, max = 5)
	private String nomAbrege;

	@NotNull
	@NotBlank
	@Length(min = 1, max = 40)
	private String nom;

	@Length(max = 40)
	private String description;

	@JsonProperty("xMin")
	private Double xMin;

	@JsonProperty("xMax")
	private Double xMax;

	@JsonProperty("yMin")
	private Double yMin;

	@JsonProperty("yMax")
	private Double yMax;

	@Length(max = 100)
	private String adresse;

	@NotNull
	@NotBlank
	@Length(min = 5, max = 5)
	private String numeroINSEE;

	private String sectionsParcelles;

	private String proprietaire;

	private String protectionJuridique;

	private Double altitude;

	private String commentaire;

	

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNomAbrege() {
		return nomAbrege;
	}

	public void setNomAbrege(String nomAbrege) {
		this.nomAbrege = nomAbrege;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getxMin() {
		return xMin;
	}

	public void setxMin(Double xMin) {
		this.xMin = xMin;
	}

	public Double getxMax() {
		return xMax;
	}

	public void setxMax(Double xMax) {
		this.xMax = xMax;
	}

	public Double getyMin() {
		return yMin;
	}

	public void setyMin(Double yMin) {
		this.yMin = yMin;
	}

	public Double getyMax() {
		return yMax;
	}

	public void setyMax(Double yMax) {
		this.yMax = yMax;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getNumeroINSEE() {
		return numeroINSEE;
	}

	public void setNumeroINSEE(String numeroINSEE) {
		this.numeroINSEE = numeroINSEE;
	}

	public String getSectionsParcelles() {
		return sectionsParcelles;
	}

	public void setSectionsParcelles(String sectionsParcelles) {
		this.sectionsParcelles = sectionsParcelles;
	}

	public String getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}

	public String getProtectionJuridique() {
		return protectionJuridique;
	}

	public void setProtectionJuridique(String protectionJuridique) {
		this.protectionJuridique = protectionJuridique;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	@Override
	public String getLabel() {
		return NAME;
	}

}
