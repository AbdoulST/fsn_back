package sogeti.cir.fsn.model;

import org.neo4j.ogm.annotation.NodeEntity;
import org.springframework.stereotype.Component;

import sogeti.cir.fsn.model.neo4j.NeoModel;

@Component
@NodeEntity
public class NatureMatiere extends NeoModel {

	private static final long serialVersionUID = 1L;
	private static final String NAME = "NatureMatiere";
	
	private String label;
	
	public NatureMatiere() {
		super();
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getName() {
		return NAME;
	}	
}