package sogeti.cir.fsn.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.model.NatureMatiere;

@Service
@Transactional
public class NatureMatiereService extends AbstractService<NatureMatiere> {
	
	public NatureMatiereService(AbstractDao<NatureMatiere> dao) {
		super(dao);
	}
}