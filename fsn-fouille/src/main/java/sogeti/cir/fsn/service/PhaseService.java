package sogeti.cir.fsn.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.dao.PhaseDAO;
import sogeti.cir.fsn.model.Phase;

@Service
@Transactional
public class PhaseService extends AbstractService<Phase> {
	
	@Autowired
	PhaseDAO phaseDao;
	
	
	public PhaseService(AbstractDao<Phase> dao) {
		super(dao);
	}



}
