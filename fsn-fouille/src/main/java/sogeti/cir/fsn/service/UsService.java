package sogeti.cir.fsn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.DescriptiveObjectError;
import sogeti.cir.fsn.api.neo4j.service.FsnServiceException;
import sogeti.cir.fsn.api.neo4j.service.IllegalOperationException;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.dao.ChantierDAO;
import sogeti.cir.fsn.dao.UsDAO;
import sogeti.cir.fsn.model.Chantier;
import sogeti.cir.fsn.model.ElementRecueilli;
import sogeti.cir.fsn.model.UniteStratigraphique;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UsService extends AbstractService<UniteStratigraphique> {
	
	@Autowired
	UsDAO usDAO;

	@Autowired
	ChantierDAO chantierDao;

	public UsService(AbstractDao<UniteStratigraphique> dao) {
		super(dao);
	}
	
	@Override
	protected void additionalSaveChecks(UniteStratigraphique objet, DescriptiveObjectError doe) {
		if(verifChantier(objet, doe)) {
			//verifIdentification(objet, doe);
		}
		//checkIdentification(objet, doe);
	}

	/**
	 * verify if the linked chantier exist or not
	 * @param objet : UniteStratigraphique 
	 */
	private boolean verifChantier(UniteStratigraphique objet, DescriptiveObjectError doe) {
		Assert.notNull(objet, "Ne peut pas être null.");
		Assert.notNull(objet.getChantier(), "Le chantier est obligatoire.");
		Long id = objet.getChantier().getId();
		Optional<Chantier> c = chantierDao.findById(id);
		if (c.isPresent()) {
			objet.setChantier(c.get());
			return true;
		}
		doe.addError("chantier", "Ce chantier n'existe pas");
		return false;
	}
	
	
	
	
	/**
	 * find if the a US with the same identification exists
	 * @param objet : US 
	 */
	private void checkIdentification(UniteStratigraphique objet, DescriptiveObjectError doe) {
		Assert.notNull(objet, "Ne peut pas être null.");
		Assert.notNull(objet.getIdentification(), "l'identification ne peut pas être null.");
		UniteStratigraphique us = usDAO.findFirstByIdentification(objet.getIdentification());
		if (us != null) {
			doe.addError("identification", "identification : ce champ doit être unique !");	
		}
	}
	
	
	
	/**
	 * verify if the identification begin by the Chantier's identification
	 * @param objet : ER 
	 */
	private void verifIdentification(UniteStratigraphique us, DescriptiveObjectError doe) {
		Assert.notNull(us, "Ne peut pas être null.");
		Assert.notNull(us.getChantier(), "le chantier ne peut pas être null.");
		if (!us.getIdentification().startsWith(
				us.getChantier().getNomAbrege()))
			doe.addError("identification", "Format de l'identifiant incorrect.");
	}

	public List<UniteStratigraphique> search(long chantierId, String containString, int limit){
		Assert.isTrue(chantierId > 0, "Id de chantier incorrect");
		Assert.notNull(containString, "Le content ne peut pas être null");
		Assert.isTrue(limit > 0, "La limite minimum est 1");
		return this.usDAO.search(containString, chantierId, limit);
		
	}

	@Override
	public void delete(long id) throws FsnServiceException {
		if(usDAO.hasER(id))
			throw new IllegalOperationException("cette us a des ER");
		super.delete(id,true);
	}
	
	

	public List<UniteStratigraphique> findByChantier(String numero) {
		return this.usDAO.findByChantierNomAbrege(numero);
	}
	
}
