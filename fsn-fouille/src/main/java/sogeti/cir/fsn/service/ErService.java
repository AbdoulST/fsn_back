package sogeti.cir.fsn.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.DescriptiveObjectError;
import sogeti.cir.fsn.api.neo4j.service.FsnServiceException;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.dao.ErDAO;
import sogeti.cir.fsn.dao.UsDAO;
import sogeti.cir.fsn.model.ElementRecueilli;
import sogeti.cir.fsn.model.UniteStratigraphique;

@Service
@Transactional
public class ErService extends AbstractService<ElementRecueilli> {
	
	public ErService(AbstractDao<ElementRecueilli> dao) {
		super(dao);
	}
	
	@Autowired
	ErDAO erDAO;
	
	@Autowired
	UsDAO usDAO;


	@Override
	protected void additionalSaveChecks(ElementRecueilli objet,
			DescriptiveObjectError doe) {
		if(verifUniteStratigraphique(objet, doe))
			verifIdentification(objet, doe);
		//checkIdentification(objet, doe);
	}

	/**
	 * find if the a Er with the same identification exists
	 * @param objet : ER 
	 */
	private void checkIdentification(ElementRecueilli objet, DescriptiveObjectError doe) {
		Assert.notNull(objet, "Ne peut pas être null.");
		Assert.notNull(objet.getIdentification(), "l'identification ne peut pas être null.");
		ElementRecueilli er = erDAO.findFirstByIdentification(objet.getIdentification());
		if (er != null) {
			doe.addError("identification", "Identification : ce champ doit être unique");	
		}
	}

	/**
	 * verify if the linked US exist or not
	 * @param objet : ER 
	 */
	private boolean verifUniteStratigraphique(ElementRecueilli objet, DescriptiveObjectError doe) {
		Assert.notNull(objet, "Ne peut pas être null.");
		Assert.notNull(objet.getUniteStratigraphique(), "l'US ne peut pas être null.");
		Long id = objet.getUniteStratigraphique().getId();
		Optional<UniteStratigraphique> us = usDAO.findById(id);
		if (us.isPresent()) {
			objet.setUniteStratigraphique(us.get());
			return true;
		}
		doe.addError("us", "This US does not exist");
		return false;
		
		
	}
	
	/**
	 * verify if the identification begin by the US's identification
	 * @param objet : ER 
	 */
	private void verifIdentification(ElementRecueilli er, DescriptiveObjectError doe) {
		Assert.notNull(er, "Ne peut pas être null.");
		Assert.notNull(er.getUniteStratigraphique(), "l'US ne peut pas être null.");
		if (!er.getIdentification().startsWith(
				er.getUniteStratigraphique().getIdentification()))
			doe.addError("identification", "Format de l'identifiant incorrect.");
	}



	@Override
	public void delete(long id) throws FsnServiceException {
		this.delete(id, true);
	}
	
	

}
