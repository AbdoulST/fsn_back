package sogeti.cir.fsn.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.JsonNode;

import sogeti.cir.fsn.FsnFouilleApplication;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.model.ElementRecueilli;
import sogeti.cir.fsn.model.UniteStratigraphique;
import sogeti.cir.fsn.service.ErService;
import sogeti.cir.fsn.test.tools.AbstractTUController;
import sogeti.cir.fsn.test.tools.ControllerAction;
import sogeti.cir.fsn.test.tools.ITBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FsnFouilleApplication.class})
@AutoConfigureMockMvc
public class ErControllerTest extends AbstractTUController<ElementRecueilli>{

	@MockBean
	private ErService service;
	
	public ErControllerTest() {
		super(ElementRecueilli.NAME+"s");
	}
	
	@Test
	public void create_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		ElementRecueilli createdEr = new ElementRecueilli();
		Date date = new Date();
		createdEr.setIdentification(data.get("identification").asText());
		createdEr.setDescription(data.get("description").asText());
		createdEr.setDateDecouverte(date);
		UniteStratigraphique us = new UniteStratigraphique();
		us.setId(data.get("uniteStratigraphique").get("id").asLong());
		createdEr.setUniteStratigraphique(new UniteStratigraphique());
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInputSchema(jrl.getJsonSchema(builder.getAction(), true))
				.withInput(data)
				.withValidInput()
				.withExpectedReturnedStatus(status().isCreated())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();
		
		Mockito.when(this.service.save(Mockito.any(ElementRecueilli.class))).thenReturn(createdEr);
		this.testFromBuilder(builder);
		
	}
	
	@Test
	public void create_no_description_fail() throws IOException, Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true, "-no-description");
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInputSchema(jrl.getJsonSchema(builder.getAction(), true))
				.withInput(data)
				.withInvalidInput()
				.withExpectedReturnedStatus(status().isBadRequest());
		
		Mockito.when(this.service.save(
				Mockito.any(ElementRecueilli.class)))
				.thenThrow(NotValidObjectException.class);
		
		this.testFromBuilder(builder);
		
		
	}
	
	@Test
	public void find_one_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		ElementRecueilli consultedEr = new ElementRecueilli();
		Date date = new Date();
		consultedEr.setIdentification(data.get("identification").asText());
		consultedEr.setDescription(data.get("description").asText());
		consultedEr.setDateDecouverte(date);
		consultedEr.setId(1L);
		UniteStratigraphique us = new UniteStratigraphique();
		us.setId(data.get("uniteStratigraphique").get("id").asLong());
		consultedEr.setUniteStratigraphique(us);
						
		ITBuilder builder = new ITBuilder();
		String[] args = {consultedEr.getId()+""};
		builder.withAction(ControllerAction.DISPLAY)
				.withPathArguments(args)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput()
				.withPostAsserts( (inputArgs, inputBody, outputBody) -> {
					Assertions.assertThat(inputArgs[0])
						.isEqualTo(outputBody.get("id").asText());
				});
		
		Mockito.when(service.findOne(Mockito.eq(consultedEr.getId()))).thenReturn(consultedEr);

		this.testFromBuilder(builder);
	}
	
	@Test
	public void find_all_page_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		ElementRecueilli consultedEr;
		ArrayList<ElementRecueilli> list = new ArrayList<ElementRecueilli>();
		for(int i = 0; i < 2; i++) {
			consultedEr = new ElementRecueilli();
			consultedEr.setId(1L);
			consultedEr.setIdentification(data.get("identification").asText());
			consultedEr.setDescription(data.get("description").asText());
			Date date = new Date ();
			consultedEr.setDateDecouverte(date);
			UniteStratigraphique us = new UniteStratigraphique();
			us.setId(data.get("uniteStratigraphique").get("id").asLong());
			consultedEr.setUniteStratigraphique(us);
			list.add(consultedEr);
		}

		Pageable page = new PageRequest(0,20);
		Page<ElementRecueilli> returnedPage = new PageImpl<ElementRecueilli>(list);
		Mockito.when(this.service.findAllByPage(page)).thenReturn(returnedPage);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.DISPLAY_ALL)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();

		this.testFromBuilder(builder);
	}
	
}