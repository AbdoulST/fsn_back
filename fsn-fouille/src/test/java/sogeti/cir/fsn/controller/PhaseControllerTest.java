package sogeti.cir.fsn.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.JsonNode;

import sogeti.cir.fsn.FsnFouilleApplication;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.model.Phase;
import sogeti.cir.fsn.service.PhaseService;
import sogeti.cir.fsn.test.tools.AbstractTUController;
import sogeti.cir.fsn.test.tools.ControllerAction;
import sogeti.cir.fsn.test.tools.ITBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FsnFouilleApplication.class})
@AutoConfigureMockMvc
public class PhaseControllerTest extends AbstractTUController<Phase>{

	
	@MockBean
	PhaseService service;
	
	public PhaseControllerTest() {
		super(Phase.NAME+"s");
	}
	
	
	@Test
	public void create_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Phase createdPhase = new Phase();
		createdPhase.setNom(data.get("nom").asText());
		createdPhase.setDescription(data.get("description").asText());
		createdPhase.setId(1L);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInputSchema(jrl.getJsonSchema(builder.getAction(), true))
				.withInput(data)
				.withValidInput()
				.withExpectedReturnedStatus(status().isCreated())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();
		
		Mockito.when(service.save(Mockito.any(Phase.class))).thenReturn(createdPhase);
		
		this.testFromBuilder(builder);
		
	}
	
	
	@Test
	public void create_no_description_fail() throws IOException, Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true, "-no-description");
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInputSchema(jrl.getJsonSchema(builder.getAction(), true))
				.withInput(data)
				.withInvalidInput()
				.withExpectedReturnedStatus(status().isBadRequest());
		
		Mockito.when(service.save(Mockito.any(Phase.class)))
				.thenThrow(NotValidObjectException.class);
		
		this.testFromBuilder(builder);
	}
	
	@Test
	public void find_one_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Phase consulted = new Phase();
		consulted.setNom(data.get("nom").asText());
		consulted.setDescription(data.get("description").asText());
		consulted.setId(1L);
		
		ITBuilder builder = new ITBuilder();
		String[] args = {consulted.getId()+""};
		builder.withAction(ControllerAction.DISPLAY)
				.withPathArguments(args)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput()
				.withPostAsserts( (inputArgs, inputBody, outputBody) -> {
					Assertions.assertThat(inputArgs[0])
						.isEqualTo(outputBody.get("id").asText());
				});
		
		Mockito.when(service.findOne(Mockito.eq(consulted.getId()))).thenReturn(consulted);

		this.testFromBuilder(builder);
	}
	
	@Test
	public void find_all_page_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Phase consulted;
		ArrayList<Phase> list = new ArrayList<Phase>();
		for(int i = 0; i < 2; i++) {
			consulted = new Phase();
			consulted.setNom(data.get("nom").asText());
			consulted.setDescription(data.get("description").asText());
			consulted.setId(i+1L);
			list.add(consulted);
		}

		Pageable page = new PageRequest(0,20);
		Page<Phase> returnedPage = new PageImpl<Phase>(list);
		Mockito.when(service.findAllByPage(page)).thenReturn(returnedPage);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.DISPLAY_ALL)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();

		this.testFromBuilder(builder);
	}

}
