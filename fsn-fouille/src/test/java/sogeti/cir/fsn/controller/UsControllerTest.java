package sogeti.cir.fsn.controller;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.JsonNode;

import sogeti.cir.fsn.FsnFouilleApplication;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.model.Chantier;
import sogeti.cir.fsn.model.UniteStratigraphique;
import sogeti.cir.fsn.service.UsService;
import sogeti.cir.fsn.test.tools.AbstractTUController;
import sogeti.cir.fsn.test.tools.ControllerAction;
import sogeti.cir.fsn.test.tools.ITBuilder;

@SpringBootTest(classes = FsnFouilleApplication.class)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class UsControllerTest extends AbstractTUController<UniteStratigraphique> {

	@MockBean
	private UsService service;
	
	public UsControllerTest() {
		super(UniteStratigraphique.NAME.toLowerCase());
	}



	@Test
	public void create_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		UniteStratigraphique createdUs = new UniteStratigraphique();
		createdUs.setIdentification(data.get("identification").asText());
		createdUs.setDescription(data.get("description").asText());
		Date date = new Date();
		createdUs.setBeginningDate(date);
		createdUs.setEndDate(date);
		createdUs.setChantier(new Chantier());
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInputSchema(jrl.getJsonSchema(builder.getAction(), true))
				.withInput(data)
				.withValidInput()
				.withExpectedReturnedStatus(status().isCreated())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();
		
		Mockito.when(this.service.save(Mockito.any(UniteStratigraphique.class))).thenReturn(createdUs);
		
		this.testFromBuilder(builder);
	}
	
	@Test
	public void find_one_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		UniteStratigraphique consultedUs = new UniteStratigraphique();
		consultedUs.setIdentification(data.get("identification").asText());
		consultedUs.setDescription(data.get("description").asText());
		Date date = new Date();
		consultedUs.setBeginningDate(date);
		consultedUs.setEndDate(date);
		consultedUs.setChantier(new Chantier());
		consultedUs.setId(1L);
		
		
		ITBuilder builder = new ITBuilder();
		String[] args = {consultedUs.getId()+""};
		builder.withAction(ControllerAction.DISPLAY)
				.withPathArguments(args)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput()
				.withPostAsserts( (inputArgs, inputBody, outputBody) -> {
					Assertions.assertThat(inputArgs[0])
						.isEqualTo(outputBody.get("id").asText());
				});
		
		Mockito.when(service.findOne(Mockito.eq(consultedUs.getId()))).thenReturn(consultedUs);

		this.testFromBuilder(builder);
	}
	
	@Test
	public void find_all_page_success() throws Exception {
		
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		UniteStratigraphique consultedUs;
		ArrayList<UniteStratigraphique> list = new ArrayList<UniteStratigraphique>();
		Chantier c = new Chantier();
		for(int i = 0; i < 2; i++) {
			consultedUs = new UniteStratigraphique();
			consultedUs.setIdentification(data.get("identification").asText());
			consultedUs.setDescription(data.get("description").asText());
			Date date = new Date();
			consultedUs.setBeginningDate(date);
			consultedUs.setEndDate(date);
			consultedUs.setId(i+1L);
			consultedUs.setChantier(c);
			list.add(consultedUs);
		}

		Pageable page = new PageRequest(0,20);
		Page<UniteStratigraphique> returnedPage = new PageImpl<UniteStratigraphique>(list);
		Mockito.when(this.service.findAllByPage(page)).thenReturn(returnedPage);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.DISPLAY_ALL)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();

		this.testFromBuilder(builder);
	}
	
	@Test
	public void create_no_chantier_fail() throws IOException, Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true, "-no-chantier");
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInputSchema(jrl.getJsonSchema(builder.getAction(), true))
				.withInput(data)
				.withInvalidInput()
				.withExpectedReturnedStatus(status().isBadRequest());
		
		Mockito.when(this.service.save(Mockito.any(UniteStratigraphique.class))).thenThrow(NotValidObjectException.class);
		
		this.testFromBuilder(builder);
	}

}
