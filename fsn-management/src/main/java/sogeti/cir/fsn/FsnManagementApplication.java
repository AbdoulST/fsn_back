package sogeti.cir.fsn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@EnableDiscoveryClient
@SpringBootApplication
public class FsnManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(FsnManagementApplication.class, args);
	}
}
