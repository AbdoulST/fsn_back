package sogeti.cir.fsn.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.stereotype.Component;

import sogeti.cir.fsn.model.neo4j.NeoModel;
 
 /**
 * @author Mehenni DJADDA
 * @createDate 30 March 2018
 **/

@Component
@NodeEntity
public class Intervenant extends NeoModel {
	private static final long serialVersionUID = 1L;
	public static final String NAME = "intervenant";
	public static final String ASSOCIATED = "Associe a";
	
	/**
	 * public static final String 
	 * Définir cette constante lorsque l'entité Traitement sera disponible
	**/
		
	@NotNull
	@NotBlank
	@Length(min = 2, max = 40)
	private String nom;
	
	@NotNull
	@NotBlank
	@Length(min = 2, max = 40)
	private String prenom;
	
	@NotNull
	@NotBlank
	@Length(min = 5, max = 20)
	private String titre;
	
	//@Length(min = 0, max = 100)
	private String adresse;
	
	//@Length(min = 0, max = 5)
	private Integer cp;
	
	//@Length(min = 0, max = 20)
	private String ville;
	
	//@Length(min = 0, max = 12)
	private Integer numeroTelephone;
	
	//@Length(min = 0, max = 30)
	private String email;
	
	//@Length(min = 0, max = 250)
	private String comment;
	 
	/** 
	 * A faire lorsque l'entite Traitement sera disponible
	* private Traitement traitement;
	**/
		
	public Intervenant() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Integer getCp() {
		return cp;
	}

	public void setCp(Integer cp) {
		this.cp = cp;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public Integer getNumeroTelephone() {
		return numeroTelephone;
	}

	public void setNumeroTelephone(Integer numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	@Override
	public String getLabel() {
		return titre;
	}

	@Override
	public String getName() {
		return NAME;
	}
}