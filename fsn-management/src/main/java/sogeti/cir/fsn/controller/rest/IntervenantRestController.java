package sogeti.cir.fsn.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.model.Intervenant;

@RestController
@RequestMapping("/intervenants")
public class IntervenantRestController extends AbstractRestController<Intervenant> {
	public IntervenantRestController(AbstractService<Intervenant> service) {
		super(service);
	}
}