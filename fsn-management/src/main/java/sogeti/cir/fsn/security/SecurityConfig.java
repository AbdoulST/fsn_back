package sogeti.cir.fsn.security;

/*import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

@Configuration
@EnableResourceServer
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
public class SecurityConfig implements ResourceServerConfigurer {

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId("fsn-gestion-site");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .httpBasic().disable()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .anonymous()
            .and().authorizeRequests()
            .antMatchers("/chantiers").hasRole("ADMIN")
            .anyRequest().permitAll()
            .and()
            .csrf().disable()
            .exceptionHandling()
            .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
            .and()
            .logout().logoutUrl("/logout").logoutSuccessUrl("/");
    }
}*/


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	

	
	private PasswordEncoder myPasswordEncoder;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		if (myPasswordEncoder == null)
			myPasswordEncoder = new BCryptPasswordEncoder(); 
		return myPasswordEncoder;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().and().csrf()
			.disable().sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and().authorizeRequests()
		.antMatchers("/actuator").permitAll()
		.antMatchers("/autoconfig").permitAll()
		.antMatchers("/beans").permitAll()
		.antMatchers("/configprops").permitAll()
		.antMatchers("/dump").permitAll()
		.antMatchers("/env").permitAll()
		.antMatchers("/flyway").permitAll()
		.antMatchers("/health").permitAll()
		.antMatchers("/info").permitAll()
		.antMatchers("/liquibase").permitAll()
		.antMatchers("/metrics").permitAll()
		.antMatchers("/mappings").permitAll()
		.antMatchers("/shutdown").denyAll()
		.antMatchers("/trace").permitAll()
		.antMatchers("/docs").permitAll()
		.antMatchers("/heapdump").permitAll()
		.antMatchers("/jolokia").permitAll()
		.antMatchers("/logfile").permitAll()
		//.antMatchers("/chantiers").hasRole("USER")
		//.antMatchers("/interventions").hasRole("ADMIN")
		.anyRequest().permitAll()//.authenticated()
		.and().logout().logoutUrl("/logout");
		http.cors();
	}
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.inMemoryAuthentication()
		.withUser("admin").password("admin").roles("ADMIN", "USER").and()
		.withUser("maurille").password("1234").roles("USER").and()
		.withUser("user").password("root").roles("VISITOR");
	
	}
	
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.applyPermitDefaultValues();
		configuration.addAllowedMethod("PUT");
		configuration.addAllowedMethod("DELETE");
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}