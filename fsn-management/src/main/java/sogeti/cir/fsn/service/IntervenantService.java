package sogeti.cir.fsn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.dao.IntervenantDAO;
import sogeti.cir.fsn.model.Intervenant;

@Service
@Transactional
public class IntervenantService extends AbstractService<Intervenant> {
	
	@Autowired
	IntervenantDAO intervenantDao;
	
	public IntervenantService(AbstractDao<Intervenant> dao) {
		super(dao);
	}
	

}