package sogeti.cir.fsn.test.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.simple.Foo;
import sogeti.cir.fsn.simple.FooController;
import sogeti.cir.fsn.simple.FooService;
import sogeti.cir.fsn.simple.TestConfig;
import sogeti.cir.fsn.test.tools.AbstractTUController;
import sogeti.cir.fsn.test.tools.ControllerAction;
import sogeti.cir.fsn.test.tools.ITBuilder;

@RunWith(SpringRunner.class)
@WebMvcTest(FooController.class)
@ContextConfiguration(classes = {TestConfig.class})
@EnableSpringDataWebSupport
public class AbstractControllerTest extends AbstractTUController<Foo> {

	@MockBean
	private FooService service;
	
	public AbstractControllerTest() {
		super(Foo.NAME);
	}
	
	// ##### find all ######
	
	@Test
	public void find_all_page_success_should_return_ok() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Foo consulted;
		ArrayList<Foo> list = new ArrayList<Foo>();
		for(int i = 0; i < 2; i++) {
			consulted = new Foo();
			consulted.setId(i+1L);
			list.add(consulted);
		}

		Pageable page = new PageRequest(0,20);
		Page<Foo> returnedPage = new PageImpl<Foo>(list);
		Mockito.when(service.findAllByPage(page)).thenReturn(returnedPage);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.DISPLAY_ALL)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();

		this.testFromBuilder(builder);
	}
	
	
	// #### find one ####
	
	@Test
	public void find_not_existing_resource_fail() throws Exception {
		String[] args = {"1"};
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.DISPLAY)
				.withPathArguments(args)
				.withExpectedReturnedStatus(status().isNotFound());
		Mockito.when(service.findOne(Mockito.eq(Long.parseLong(builder.getPathArguments()[0]))))
				.thenThrow(ResourceNotFoundException.class);
		this.testFromBuilder(builder);
	}
	
	
	@Test
	public void find_one_success_should_return_ok() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Foo consulted = new Foo();
		consulted.setId(1L);
		
		ITBuilder builder = new ITBuilder();
		String[] args = {consulted.getId()+""};
		builder.withAction(ControllerAction.DISPLAY)
				.withPathArguments(args)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput()
				.withPostAsserts( (inputArgs, inputBody, outputBody) -> {
					Assertions.assertThat(inputArgs[0])
						.isEqualTo(outputBody.get("id").asText());
				});
		
		Mockito.when(service.findOne(Mockito.eq(consulted.getId()))).thenReturn(consulted);

		this.testFromBuilder(builder);
	}
	
	// ##### create #####
	
	@Test
	public void create_empty_input_should_return_bad_request() throws Exception {
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withExpectedReturnedStatus(status().isBadRequest());
		Mockito.when(this.service.save(Mockito.any(Foo.class))).thenThrow(NotValidObjectException.class);
		this.testFromBuilder(builder);
	}
	
	@Test
	public void create_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Foo created = new Foo();
		created.setId(1L);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInputSchema(jrl.getJsonSchema(builder.getAction(), true))
				.withInput(data)
				.withValidInput()
				.withExpectedReturnedStatus(status().isCreated())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();
		
		Mockito.when(this.service.save(Mockito.any(Foo.class))).thenReturn(created);
		
		this.testFromBuilder(builder);
		
	}
	
	@Test
	public void create_not_valid_object_should_return_bad_request() throws IOException, Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInput(data)
				.withExpectedReturnedStatus(status().isBadRequest());
		
		Mockito.when(this.service.save(Mockito.any(Foo.class)))
				.thenThrow(NotValidObjectException.class);
		
		this.testFromBuilder(builder);
	}
	
	// ###### delete ######
	
	@Test
	public void delete_success_should_return_no_content() throws Exception {
		String[] args = {"1"};
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.REMOVE)
				.withPathArguments(args)
				.withExpectedReturnedStatus(status().isNoContent());

		Mockito.doNothing().when(service)
							.delete(Mockito.eq(Long.parseLong(builder.getPathArguments()[0])));
		this.testFromBuilder(builder);	
	}
	
	@Test
	public void delete_not_existing_resource_should_return_not_found() throws Exception {
		String[] args = {"1"};
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.REMOVE)
				.withPathArguments(args)
				.withExpectedReturnedStatus(status().isNotFound());

		Mockito.doThrow(ResourceNotFoundException.class).when(service)
							.delete(Mockito.eq(Long.parseLong(builder.getPathArguments()[0])));
		this.testFromBuilder(builder);
	}
	

}
