package sogeti.cir.fsn.test.service;

import java.util.Optional;

import org.junit.Test;
import org.mockito.Mockito;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.model.neo4j.NeoModel;

class Foo extends NeoModel {
	public static final String NAME = "Foo";
	@Override
	public String getLabel() {
		return NAME;
	}
}
interface FooDao extends AbstractDao<Foo> {}
class FooService extends AbstractService<Foo> {

	public FooService(AbstractDao<Foo> dao) {
		super(dao);
	}
}

public class AbstractServiceTest {
	
	private FooDao dao;
	private FooService service;
	
	/**
	@BeforeMethod
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }
    **/
	
	public AbstractServiceTest() {
		this.dao = Mockito.mock(FooDao.class);
		this.service = new FooService(dao);
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void find_unexisting_object_should_return_exception() throws ResourceNotFoundException {
		Mockito.when(dao.findById(Mockito.any(Long.class))).thenReturn(Optional.empty());
		service.findOne(1L);
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void delete_unexisting_object_should_return_exception() throws ResourceNotFoundException {
		Mockito.when(dao.findById(Mockito.any(Long.class))).thenReturn(Optional.empty());
		service.findOne(1L);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void save_null_object_should_return_exception() throws ResourceNotFoundException, NotValidObjectException {
		service.save(null);
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void save_object_with_unexisting_id_should_return_exception() throws ResourceNotFoundException, NotValidObjectException {
		Foo t = Mockito.mock(Foo.class);
		t.setId(1L);
		Mockito.when(dao.findById(Mockito.any(Long.class))).thenReturn(Optional.empty());
		service.save(t);
	}
	
	/**
	@Test(expected = NotValidObjectException.class)
	public void save_not_valid_object_should_return_exception() throws ResourceNotFoundException, NotValidObjectException {
		T t = Mockito.mock(type);
		t.setId(1L);
		Mockito.when(getDao().findOne(Mockito.any(Long.class))).thenReturn(t);
		Set<ConstraintViolation<T>> errs = new HashSet<ConstraintViolation<T>>();
		errs.add(Mockito.mock(ConstraintViolation.class));
		Mockito.when(Mockito.mock(Validator.class).validate()).thenReturn(errs);
		getService().save(t);
	}
	**/
	
}
