package sogeti.cir.fsn.test.model;

import org.junit.Test;

import sogeti.cir.fsn.model.neo4j.NeoModel;
import org.assertj.core.api.Assertions;

public class NeoModelTest {

	class Foo extends NeoModel {
		public static final String NAME = "Foo";
		@Override
		public String getLabel() {
			return NAME;
		}
	}
	
	class Bar extends NeoModel {
		public static final String NAME = "Bar";
		@Override
		public String getLabel() {
			return NAME;
		}
	}
	
	@Test
	public void default_data_ok() {
		Foo f = new Foo();
		Assertions.assertThat(f.getId() == null).isTrue();
		Assertions.assertThat(f.getLabel().equals(Foo.NAME)).isTrue();
		Assertions.assertThat(f.getName().equals(Bar.NAME));
	}
	
	@Test
	public void set_valid_id_shoud_success() {
		Foo f = new Foo();
		Long nid = 1L;
		f.setId(nid);
		Assertions.assertThat(f.getId() == nid).isTrue();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void set_negativ_id_should_fail() {
		Foo f = new Foo();
		Long nid = -1L;
		f.setId(nid);
	}
	
	@Test
	public void equals_with_same_class_and_id_should_success() {
		Long id = 1L;
		Foo f1 = new Foo();
		Foo f2 = new Foo();
		f1.setId(id);
		f2.setId(id);
		Assertions.assertThat(f1.equals(f2)).isTrue();
	}
	
	@Test
	public void equals_with_null_object_should_fail() {
		Long id = 1L;
		Foo f1 = new Foo();
		f1.setId(id);
		Assertions.assertThat(f1.equals(null)).isFalse();
	}
	
	@Test
	public void equals_with_same_class_different_id_should_fail() {
		Foo f1 = new Foo();
		Foo f2 = new Foo();
		f1.setId(1L);
		f2.setId(2L);
		Assertions.assertThat(f1.equals(f2)).isFalse();
	}
	
	@Test
	public void equals_with_different_class_same_id_should_fail() {
		Long id = 1L;
		Foo f = new Foo();
		Bar b = new Bar();
		f.setId(id);
		b.setId(id);
		Assertions.assertThat(f.equals(b)).isFalse();
	}
}
