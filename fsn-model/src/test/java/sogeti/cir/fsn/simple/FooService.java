package sogeti.cir.fsn.simple;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;

@Service
@Transactional
public class FooService extends AbstractService<Foo> {

	public FooService(AbstractDao<Foo> dao) {
		super(dao);
	}
}
