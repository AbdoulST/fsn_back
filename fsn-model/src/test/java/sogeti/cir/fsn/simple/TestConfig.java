package sogeti.cir.fsn.simple;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("sogeti.cir.fsn.simple")
public class TestConfig {

}
