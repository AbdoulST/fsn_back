package sogeti.cir.fsn.simple;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;

@RestController
@RequestMapping("/"+Foo.NAME)
public class FooController extends AbstractRestController<Foo> {

	public FooController(AbstractService<Foo> service) {
		super(service);
	}
	
}
