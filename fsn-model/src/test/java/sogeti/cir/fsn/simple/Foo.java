package sogeti.cir.fsn.simple;

import sogeti.cir.fsn.model.neo4j.NeoModel;

public class Foo extends NeoModel {
	public static final String NAME = "foo";
	@Override
	public String getLabel() {
		return NAME;
	}
}
