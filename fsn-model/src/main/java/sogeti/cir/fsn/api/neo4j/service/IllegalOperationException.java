package sogeti.cir.fsn.api.neo4j.service;

public class IllegalOperationException extends FsnServiceException {

	private String message;
	
	public IllegalOperationException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}
	
	
}
