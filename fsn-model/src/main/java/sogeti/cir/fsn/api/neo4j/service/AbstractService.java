package sogeti.cir.fsn.api.neo4j.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Optional;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.neo4j.driver.v1.exceptions.ClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.common.reflect.TypeToken;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.neo4j.NeoModel;

public class AbstractService<T extends NeoModel> {

	protected Validator validator;

	protected final Logger log = LoggerFactory.getLogger(getClass());
	protected AbstractDao<T> dao;

	private Class<T> clazz;

	public AbstractService(AbstractDao<T> dao) {
		this.dao = dao;
		this.clazz = (Class<T>) new TypeToken<T>(getClass()) {
			private static final long serialVersionUID = 1L;
		}.getRawType();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	    this.validator = factory.getValidator();
	}

	public AbstractService(AbstractDao<T> dao, Validator validator ) {
		this(dao);
		this.validator = validator;
	}

    @Transactional
    public Page<T> findAllByPage(Pageable page) {
           return dao.findAll(page);
    }

	@Transactional
	public Iterable<T> findAll() {
		return dao.findAll();
	}

	@Transactional
	public Page<T> search(String search, Pageable page) throws Exception{
		String label = clazz.getConstructor().newInstance().getLabel();
		return this.dao.search(label, search, page);
		
	}

	@Transactional
	public T findOne(Long id) throws ResourceNotFoundException {
		Optional<T> obj = dao.findById(id);
		if(!obj.isPresent())
			throw new ResourceNotFoundException();
		return obj.get();
	}

	@Transactional
	public T save(T objet) throws NotValidObjectException, ResourceNotFoundException {
		Assert.notNull(objet, "Ne peut pas être null.");
		if(objet.getId() != null)			// si c'est une modification 
			this.findOne(objet.getId()); 	// on s'assure que l'objet existe en base
		DescriptiveObjectError doe = new DescriptiveObjectError();
		this.validateObject(objet, doe);
		this.additionalSaveChecks(objet, doe);
		T t = null;
		try {
			if(doe.getErrors().isEmpty())
				t = dao.save(objet);
		} catch(ClientException e) {
			CypherConstraintViolation ccv = new CypherConstraintViolation(e);
			doe.addError(ccv.getProperty(), ccv.getProperty() + " : " + ccv.getErrorMessage());
		}
		if(!doe.getErrors().isEmpty())
			throw new NotValidObjectException(doe.getErrors());
		return t;
	}
	
	protected void additionalSaveChecks(T objet,
			DescriptiveObjectError doe) {}

	@Transactional
	public void delete(long id, boolean detach) throws FsnServiceException {
		Assert.isTrue(id >= 0, "This id is not correct");
		try {
			if(detach)
				dao.deleteById(id);
			else
				dao.safeDeleteById(id);
		}catch(ClientException | DataIntegrityViolationException e) {
			CypherConstraintViolation ccv = new CypherConstraintViolation(e);
			throw new IllegalOperationException(ccv.getErrorMessage());
		}
		
	}
	
	public void delete(long id) throws FsnServiceException {
		this.delete(id, false);
	}

	@Transactional
	public long count() {
		return dao.count();
	}


	public AbstractDao<T> getDao() {
		return dao;
	}
	
	@Transactional
	public void validateObject(T object, DescriptiveObjectError doe) throws NotValidObjectException {
		Set<ConstraintViolation<T>> errors = validator.validate(object);
		if(errors.size() > 0) {
			List<String> l = new ArrayList<String>();
			for(ConstraintViolation cv : errors)
				doe.addError(cv.getPropertyPath().toString(), cv.getMessage());
		}
	}

}