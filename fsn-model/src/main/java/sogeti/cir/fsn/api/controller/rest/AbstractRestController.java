package sogeti.cir.fsn.api.controller.rest;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.FsnServiceException;
import sogeti.cir.fsn.api.neo4j.service.IllegalOperationException;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.model.neo4j.NeoModel;

public class AbstractRestController<T extends NeoModel> {

	private static final int DEFAULT_PAGE_SIZE = 20; 
	private static final int DEFAULT_PAGE_NUMBER =1; 
	private static final Pageable DEFAULT_PAGE = PageRequest.of(DEFAULT_PAGE_NUMBER,DEFAULT_PAGE_SIZE); ; 
	protected final Logger log = LoggerFactory.getLogger(getClass());
	protected AbstractService<T> service;

	public AbstractRestController(AbstractService<T> service) {
		this.service = service;
	}

    @GetMapping
    public Page<T> findAllByPage(Pageable page) {
        return service.findAllByPage(page);
    }

	@GetMapping("/{id}")
	public ResponseEntity getOne(@PathVariable(required = true) Long id) {
		T result;
		try {
			result = service.findOne(id);
		} catch (ResourceNotFoundException e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(result,HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity update(@RequestBody(required = true) T objet, @PathVariable(required = true) Long id) {
		T saved;
		try {
			Assert.notNull(objet, "Ne peut pas être null.");
			Assert.isTrue(id.equals(objet.getId()), "L'id de l'url ne correspond pas à celui de l'objet envoyé.");
			
			saved = service.save(objet);
		}catch(ResourceNotFoundException e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.NOT_FOUND);
		}catch(NotValidObjectException e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
		}catch(IllegalArgumentException e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<T>(saved,HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity create(@RequestBody(required = true) T objet){
		T obj;
		try {
			Assert.isNull(objet.getId(), "L'identifiant doit être null");
			Assert.notNull(objet, "Ne peut pas être null.");
			
			obj = service.save(objet);
		} catch (NotValidObjectException e) {
			return new ResponseEntity<Map<String,List<String>>>(e.getErrors(),HttpStatus.BAD_REQUEST);
		} catch (ResourceNotFoundException e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.FORBIDDEN);
		}catch(IllegalArgumentException e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<T>(obj, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable(required = true) Long id) throws FsnServiceException {
		try {
			service.delete(id);
		} catch (ResourceNotFoundException e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.NOT_FOUND);
		} catch (IllegalOperationException e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/search")
	public ResponseEntity search(@RequestParam(required = true) String containString,
							Pageable page) throws Exception {
		try {
		Pageable p = page == null ? DEFAULT_PAGE : page;
		
			return new ResponseEntity<Page<T>>(this.service.search(containString, p),HttpStatus.OK);
		} catch (Exception e) {
			return new ErrorResponseEntity(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	public AbstractService<T> getService() {
		return service;
	}
}