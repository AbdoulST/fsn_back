package sogeti.cir.fsn.api.neo4j.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DescriptiveObjectError {
	
	private Map<String, List<String>> errors;
	
	public DescriptiveObjectError() {
		this.errors = new HashMap<String, List<String>>();
	}
	
	public void addError(String property, String error) {
		List<String> l = errors.get(property);
		if(l == null) 
			l = new ArrayList<String>();
		l.add(error);
		addError(property, l);
		
	}
	
	public void addError(String property, List<String> propErrors) {
		this.errors.put(property, propErrors);
	}

	public Map<String, List<String>> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, List<String>> errors) {
		this.errors = errors;
	}

}
