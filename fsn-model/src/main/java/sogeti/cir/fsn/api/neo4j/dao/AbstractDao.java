package sogeti.cir.fsn.api.neo4j.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import sogeti.cir.fsn.model.neo4j.NeoModel;

@NoRepositoryBean
public interface AbstractDao<T extends NeoModel> extends Neo4jRepository<T, Long> {

	@Query(value = "MATCH (n) WHERE (any(prop in keys(n) where n[prop] CONTAINS {search})) AND {label} IN labels(n) RETURN n",
			countQuery = "MATCH (n) WHERE (any(prop in keys(n) where n[prop] CONTAINS {search})) AND {label} IN labels(n) RETURN count(n)")
	public Page<T> search(@Param("label") String label, @Param("search") String search, Pageable page);
	
	@Query("MATCH (n) WHERE id(n) = {id} DELETE n")
	public void safeDeleteById(@Param("id") long id);
	
}