package sogeti.cir.fsn.api.neo4j.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.neo4j.ogm.exception.CypherException;
import org.springframework.dao.DataIntegrityViolationException;

public class CypherConstraintViolation {

	public enum ConstraintType {
		UNIQUE,
		LINKED
	}
	
	private String property;
	private ConstraintType violatedConstraint;
	
	public CypherConstraintViolation(Exception e) throws RuntimeException {
		String message = e.getMessage();
		Pattern propertyPattern = Pattern.compile(".*property `(.*)`");
		Matcher matcher = propertyPattern.matcher(message);
		if (matcher.find())
			this.property = matcher.group(1);
		if(message.contains("already exists"))
			this.violatedConstraint = ConstraintType.UNIQUE;
		else if(message.contains("still has relationships")) {
			this.violatedConstraint = ConstraintType.LINKED;
		}
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public ConstraintType getViolatedConstraint() {
		return violatedConstraint;
	}

	public void setViolatedConstraint(ConstraintType violatedConstraint) {
		this.violatedConstraint = violatedConstraint;
	}
	
	public String getErrorMessage() {
		switch(this.getViolatedConstraint()) {
			case UNIQUE : return "ce champ doit être unique";
			case LINKED : return "ce noeud a des relations";
			default : return "unknow database error";
		}
	}
	
}
