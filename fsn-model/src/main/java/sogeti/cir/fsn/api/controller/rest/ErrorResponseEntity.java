package sogeti.cir.fsn.api.controller.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ErrorResponseEntity extends ResponseEntity<RestErrorResponse>{

	private RestErrorResponse body;
	public ErrorResponseEntity(String message, HttpStatus status) {
		super(new RestErrorResponse(message),status);
	}

}
