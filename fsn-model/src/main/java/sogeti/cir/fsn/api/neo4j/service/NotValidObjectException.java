package sogeti.cir.fsn.api.neo4j.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotValidObjectException extends FsnServiceException {

	private Map<String,List<String>> errors;
	
	public NotValidObjectException() {
		this.errors = new HashMap<String, List<String>>();
	}
	
	public NotValidObjectException(Map errors) {
		this.errors = errors;
	}

	public Map<String, List<String>> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, List<String>> errors) {
		this.errors = errors;
	}
}
