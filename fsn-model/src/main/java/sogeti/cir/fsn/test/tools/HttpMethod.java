package sogeti.cir.fsn.test.tools;

public enum HttpMethod {
	POST,
	DELETE,
	PUT,
	GET;
			
}
