package sogeti.cir.fsn.test.tools;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class JsonRessourceLoader {

	private final String DEFAULT_RESOURCES_PATH = "src/test/resources/schemas";
	private final String RESOURCES_PATH;
	private final String RESOURCES_OBJ_PATH;
	private final Path ABSOLUTE_REOURCE_OBJ_PATH;
	private final String RESOURCE_NAME;
	
	public JsonRessourceLoader (String resourceName, String path) {
		this.RESOURCES_PATH = path == null ? DEFAULT_RESOURCES_PATH : path;
		this.RESOURCE_NAME = resourceName;
		this.RESOURCES_OBJ_PATH = RESOURCES_PATH + "/" + this.RESOURCE_NAME;
		this.ABSOLUTE_REOURCE_OBJ_PATH = Paths.get(RESOURCES_OBJ_PATH);
	}
	
	public JsonRessourceLoader (String resourceName) {
		this(resourceName, null);
		
	}

	public String getJSONFilePath(boolean isRequest, ControllerAction ca, boolean isSchema, String suffix) throws IOException {
		return ABSOLUTE_REOURCE_OBJ_PATH.toRealPath() + "/"
			+ ca + "-"
			+ (isRequest ? "req" : "res")
			+ (isSchema ? "-schema" : "") + suffix +".json";
	}
	
	public JsonNode getJsonRessouce(ControllerAction ca, boolean isRequest) throws IOException {
		return this.getJsonRessouce(ca, isRequest, false, "");
	}

	public JsonNode getJsonRessouce(ControllerAction ca, boolean isRequest, String suffix) throws IOException {
		return getJsonRessouce(ca, isRequest, false, suffix);
	}
	
	private JsonNode getJsonRessouce(ControllerAction ca, boolean isRequest, boolean isSchema, String suffix) throws IOException {
		return JsonLoader.fromPath(getJSONFilePath(isRequest, ca, isSchema, suffix));
	}
	
	
	public JsonSchema getJsonSchema(ControllerAction ca, boolean isRequest) throws IOException, ProcessingException {
		return this.getJsonSchema(ca, isRequest, "");
	}
	
	public JsonSchema getJsonSchema(ControllerAction ca, boolean isRequest, String suffix) throws IOException, ProcessingException {
		final JsonNode reqFileSchema = getJsonRessouce(ca, isRequest, true, suffix);
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		return factory.getJsonSchema(reqFileSchema);
	}
	
}
