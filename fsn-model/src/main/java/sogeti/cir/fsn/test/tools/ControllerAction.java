package sogeti.cir.fsn.test.tools;

public enum ControllerAction {
	CREATE("create"),
	REMOVE("remove"),
	UPDATE("update"),
	DISPLAY("display"),
	DISPLAY_ALL("display-all");
	
	private String name = "";
	   
	ControllerAction(String name){
	    this.name = name;
	}
	   
	public String toString(){
		return name;
	}
			
}
