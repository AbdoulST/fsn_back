package sogeti.cir.fsn.test.tools;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;

import sogeti.cir.fsn.model.neo4j.NeoModel;


public abstract class AbstractTUController<T extends NeoModel> {
	
	protected final String URL_PREFIX;
	protected final JsonRessourceLoader jrl;
	
	@Autowired
	private MockMvc mockMvc;
	
	protected AbstractTUController(String resourceName) {
		this.URL_PREFIX = "/"+resourceName;
		this.jrl = new JsonRessourceLoader(resourceName);
	}
	
	protected void testFromBuilder(ITBuilder builder) throws Exception {
		JsonNode jsonInput = builder.inputAsJson();
		
		// #### vérification de la request
		assertSchema(builder.getInputSchema(), jsonInput, builder.isValidInput());
		if(builder.getPreAsserts() != null)
			builder.getPreAsserts().asserts(builder.getPathArguments(),jsonInput);

		// #### request ####
		String url = URL_PREFIX;
		for(String s : builder.getPathArguments())
			url += "/" + s;
		MockHttpServletRequestBuilder requestBuilder = null;
		switch(builder.getMethod()) {
		case POST : requestBuilder = MockMvcRequestBuilders.post(url); break;
		case PUT : requestBuilder = MockMvcRequestBuilders.put(url); break;
		case DELETE : requestBuilder = MockMvcRequestBuilders.delete(url); break;
		case GET : requestBuilder = MockMvcRequestBuilders.get(url); break;
		}
		requestBuilder = requestBuilder.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);
		if(builder.getInput() != null)
			requestBuilder.content(builder.getInput());
		MvcResult result = mockMvc.perform(requestBuilder).andExpect(builder.getExpectedReturnedStatus()).andReturn();
		JsonNode jsonResult = getJsonNodeResult(result);

		// #### vérification de la response
		assertSchema(builder.getOutputSchema(), jsonResult, builder.isValidOutput());
		
		if(builder.getPostAsserts() != null)
			builder.getPostAsserts().asserts(builder.getPathArguments(), jsonInput, jsonResult);
	}
	
	private void assertSchema(JsonSchema schema, JsonNode data, boolean shouldBeValid) throws ProcessingException {
		if(schema != null) {
			if(data == null)
				Assertions.assertThat(shouldBeValid).isFalse();
			ProcessingReport validationReport = schema.validate(data);
			if(shouldBeValid)
				Assertions.assertThat(validationReport.isSuccess()).isTrue();
			else
				Assertions.assertThat(validationReport.isSuccess()).isFalse();
		} else {
			Assertions.assertThat(shouldBeValid).isTrue();
		}
	}
	
	protected JsonNode getJsonNodeResult(MvcResult result) throws IOException{
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();
		if(resultBody.isEmpty())
			return null;
		ObjectMapper om = new ObjectMapper();
		return om.readTree(resultBody);
	}
	
	
	// ##### update ######
	
	@Test
	public void update_different_path_and_object_id_should_return_bad_request() throws Exception {
		ObjectNode input = JsonNodeFactory.instance.objectNode();
		input.put("id", 1L);
		final Long pathid = input.get("id").asLong()+1;
		String[] args = {pathid+""};
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.UPDATE)
				.withPathArguments(args)
				.withInput(input)
				.withPreAsserts( (inputArgs, inputBody) -> {
					Assertions.assertThat(inputArgs[0].equals(inputBody.get("id").asText())).isFalse();
				})
				.withValidInput()
				.withExpectedReturnedStatus(status().isBadRequest());
		this.testFromBuilder(builder);
	}
	
	@Test
	public void update_empty_object_should_return_bad_request() throws Exception {
		final Long pathid = 1L;
		String[] args = {pathid+""};
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.UPDATE)
				.withPathArguments(args)
				.withValidInput()
				.withExpectedReturnedStatus(status().isBadRequest());
		this.testFromBuilder(builder);
	}
	
	// ##### create #####
	
	@Test
	public void create_empty_objetc_should_return_bad_request() throws Exception {
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withValidInput()
				.withExpectedReturnedStatus(status().isBadRequest());
		this.testFromBuilder(builder);
	}
	
	@Test
	public void create_objetc_with_id_should_return_bad_request() throws Exception {
		ObjectNode input = JsonNodeFactory.instance.objectNode();
		input.put("id", 1L);
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withValidInput()
				.withInput(input)
				.withExpectedReturnedStatus(status().isBadRequest());
		this.testFromBuilder(builder);
	}
	
}
