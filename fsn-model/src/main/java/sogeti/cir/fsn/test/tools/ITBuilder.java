package sogeti.cir.fsn.test.tools;

import java.io.IOException;

import org.springframework.test.web.servlet.ResultMatcher;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.main.JsonSchema;

public class ITBuilder {
	
	
	public interface PreAsserts {
		void asserts(String[] inputArgs, JsonNode inputBody);
	}
	
	public interface PostAsserts {
		void asserts(String[] inputArgs, JsonNode inputBody, JsonNode outputBody);
	}
	
	
	private ControllerAction action;
	private String[] pathArguments;
	private HttpMethod method;

	private String input;
	private boolean isValidInput;
	private JsonSchema inputSchema;
	private PreAsserts preAsserts;

	private ResultMatcher expectedReturnedStatus;
	
	private JsonSchema outputSchema;
	private boolean isValidOutput;
	private PostAsserts postAsserts;
	
	public ITBuilder() {
		this.pathArguments = new String[0];
		this.isValidInput = true;
		this.isValidOutput = true;
	}
	
	public ControllerAction getAction() {
		return action;
	}
	public ITBuilder withAction(ControllerAction action) {
		this.action = action;
		
		switch(action) {
		case CREATE : this.method = HttpMethod.POST; break;
		case UPDATE : this.method = HttpMethod.PUT; break;
		case REMOVE : this.method = HttpMethod.DELETE; break;
		case DISPLAY : this.method = HttpMethod.GET; break;
		case DISPLAY_ALL : this.method = HttpMethod.GET; break;
		}
		
		return this;
	}
	
	public JsonNode inputAsJson() throws JsonProcessingException, IOException {
		if(input == null  || input.isEmpty())
			return null;
		ObjectMapper om = new ObjectMapper();
		return om.readTree(input);
	}
	
	public String getInput() {
		return input;
	}
	
	public ITBuilder withInput(JsonNode input) {
		this.input = input.toString();
		return this;
	}
	
	public boolean isValidInput() {
		return isValidInput;
	}
	public ITBuilder withValidInput() {
		this.isValidInput = true;;
		return this;
	}
	public ITBuilder withInvalidInput() {
		this.isValidInput = false;
		return this;
	}
	
	public JsonSchema getInputSchema() {
		return inputSchema;
	}
	public ITBuilder withInputSchema(JsonSchema inputSchema) {
		this.inputSchema = inputSchema;
		return this;
	}
	public ResultMatcher getExpectedReturnedStatus() {
		return expectedReturnedStatus;
	}
	public ITBuilder withExpectedReturnedStatus(ResultMatcher expectedReturnedStatus) {
		this.expectedReturnedStatus = expectedReturnedStatus;
		return this;
	}
	public JsonSchema getOutputSchema() {
		return outputSchema;
	}
	public ITBuilder withOutputSchema(JsonSchema outputSchema) {
		this.outputSchema = outputSchema;
		return this;
	}
	public boolean isValidOutput() {
		return isValidOutput;
	}
	public ITBuilder withValidOutput() {
		this.isValidOutput = true;
		return this;
	}
	public ITBuilder withInvalidOutput() {
		this.isValidOutput = false;
		return this;
	}
	
	public PreAsserts getPreAsserts() {
		return preAsserts;
	}
	public ITBuilder withPreAsserts(PreAsserts preAsserts) {
		this.preAsserts = preAsserts;
		return this;
	}
	public PostAsserts getPostAsserts() {
		return postAsserts;
	}
	public ITBuilder withPostAsserts(PostAsserts postAsserts) {
		this.postAsserts = postAsserts;
		return this;
	}
	
	public String[] getPathArguments() {
		return pathArguments;
	}
	public ITBuilder withPathArguments(String[] pathArguments) {
		this.pathArguments = pathArguments;
		return this;
	}
	
	public HttpMethod getMethod() {
		return method;
	}
	public void withMethod(HttpMethod method) {
		this.method = method;
	}
	
	
}
