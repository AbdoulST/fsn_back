package sogeti.cir.fsn.model.neo4j;

import java.io.Serializable;

import org.neo4j.ogm.annotation.GraphId;

import com.fasterxml.jackson.annotation.JsonIgnore;


public abstract class NeoModel implements Comparable<NeoModel>, Serializable {

	private static final long serialVersionUID = 1L;

	@GraphId
	private Long id;
	
	@JsonIgnore
	public abstract String getLabel();

	@JsonIgnore
	public String getName() {
		return this.getLabel().toLowerCase();
	}

	public Long getId() {
		return id;
	}
	
	public final void setId(Long id) {
		if(id != null && id < 0)
			throw new IllegalArgumentException("id can't be lower than 0");
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!this.getClass().getName().equals(obj.getClass().getName()))
			return false;
		return this.getId() == ((NeoModel) obj).getId();
	}

	@Override
	public int compareTo(NeoModel arg0) {
		return this.getId() == arg0.getId() ? 0 : this.getId() < arg0.getId() ? -1 : 1;
	}

}