package sogeti.cir.fsn.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.model.neo4j.Chantier;
import sogeti.cir.fsn.model.neo4j.OperationArcheologique;
import sogeti.cir.fsn.service.OAService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class OARestControllerTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private OAService service;

	final String RESOURCE_PATH = "src/test/resources/schemas/operationArcheologique";
	final Path REAL_PATH = Paths.get(RESOURCE_PATH);
	final Long UNKNOWN_ID = Long.MAX_VALUE;
	
	@Test
	public void createOA_success() throws Exception {

		final JsonNode reqFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-req-create.json");
		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-create.json");

		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final JsonSchema requestSchema = factory.getJsonSchema(reqFileSchema);

		Assertions.assertThat(requestSchema.validate(data).isSuccess()).isTrue();

		final String inputJson = data.toString();
		final String uri = "/oa";

		OperationArcheologique createdOA = new OperationArcheologique();
		createdOA.setIdentifiant(data.get("identifiant").asText());
		createdOA.setDesignationDecree(data.get("designationDecree").asText());
		createdOA.setPrescriptionDecree(data.get("prescriptionDecree").asText());
		Date date = new Date();
		createdOA.setCreatedDate(date);
		createdOA.setLastModifiedDate(date);
		Mockito.when(service.save(Mockito.any(OperationArcheologique.class))).thenReturn(createdOA);

		RequestBuilder rb = MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON).content(inputJson)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isCreated()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();

		final JsonNode resFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-res-create.json");
		final JsonSchema responseSchema = factory.getJsonSchema(resFileSchema);
		ObjectMapper om = new ObjectMapper();
		System.out.println(resultBody);

		JsonNode jsonResult = om.readTree(resultBody);

		Assertions.assertThat(responseSchema.validate(jsonResult).isSuccess()).isTrue();

	}

	@Test
	public void createOA_bad_request() throws Exception {

		final JsonNode reqFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-req-create.json");
		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/bad-req-create.json");

		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final JsonSchema requestSchema = factory.getJsonSchema(reqFileSchema);

		Assertions.assertThat(requestSchema.validate(data).isSuccess()).isFalse();

		final String uri = "/oa";

		Mockito.when(service.save(Mockito.any(OperationArcheologique.class))).thenReturn(null);

		RequestBuilder rb = MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isBadRequest()).andReturn();
		MockHttpServletResponse response = result.getResponse();

		Assertions.assertThat(response.getContentAsString().isEmpty());

	}

	@Test
	public void findOA_byId_success() throws Exception {

		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-create.json");
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final Long testId = 18L;
		final String uri = "/oa/" + testId;
		
		OperationArcheologique consultedOA = new OperationArcheologique();
		consultedOA.setIdentifiant(data.get("identifiant").asText());
		consultedOA.setDesignationDecree(data.get("designationDecree").asText());
		consultedOA.setPrescriptionDecree(data.get("prescriptionDecree").asText());
		consultedOA.setId(testId);
		Date date = new Date();
		consultedOA.setCreatedDate(date);
		consultedOA.setLastModifiedDate(date);

		Mockito.when(service.findOne(Mockito.eq(testId))).thenReturn(consultedOA);

		RequestBuilder rb = MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isOk()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();

		final JsonNode resFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-res-read.json");
		final JsonSchema responseSchema = factory.getJsonSchema(resFileSchema);

		ObjectMapper om = new ObjectMapper();
		JsonNode jsonResult = om.readTree(resultBody);

		long consultedId = jsonResult.findValue("id").asLong();

		Assertions.assertThat(responseSchema.validate(jsonResult).isSuccess()).isTrue();
		Assertions.assertThat(consultedId).isEqualTo(testId);

	}

	@Test
	public void findChantier_byId_fail() throws Exception {

		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-create.json");
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final Long testBadId = 25L;
		final String uri = "/oa/" + testBadId;

		Mockito.when(service.findOne(Mockito.eq(testBadId))).thenThrow(ResourceNotFoundException.class);

		RequestBuilder rb = MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isNotFound()).andReturn();

		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();

		Assertions.assertThat(resultBody).isNullOrEmpty();

	}
	
	@Test
	public void deleteOA_success() throws Exception {

		final Long testId = 18L;
		final String uri = "/oa/" + testId;

		Mockito.when(service.delete(Mockito.eq(testId))).thenReturn(true);

		RequestBuilder rb = MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isNoContent()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBodyDel = response.getContentAsString();
		
		Assertions.assertThat(resultBodyDel).isNullOrEmpty();

	}
	
	@Test
	public void deleteChantier_notFound() throws Exception {

		final Long testId = 18L;
		final String uri = "/oa/" + testId;

		Mockito.when(service.delete(Mockito.eq(testId))).thenReturn(false);

		RequestBuilder rb = MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isNoContent()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBodyDel = response.getContentAsString();
		
		Assertions.assertThat(resultBodyDel).isNullOrEmpty();

	}
	
	@Test
	public void updateOA_success() throws Exception {

		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-create.json");
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final Long testId = 18L;
		final String uri = "/oa/" + testId;

		OperationArcheologique consultedOA = new OperationArcheologique();
		consultedOA.setIdentifiant(data.get("identifiant").asText());
		consultedOA.setDesignationDecree(data.get("designationDecree").asText());
		consultedOA.setPrescriptionDecree(data.get("prescriptionDecree").asText());
		consultedOA.setId(testId);
		Date date = new Date();
		consultedOA.setCreatedDate(date);
		consultedOA.setLastModifiedDate(date);

		Mockito.when(service.findOne(Mockito.eq(testId))).thenReturn(consultedOA);

		RequestBuilder rb = MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isOk()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();

		final JsonNode resFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-res-read.json");
		final JsonSchema responseSchema = factory.getJsonSchema(resFileSchema);

		ObjectMapper om = new ObjectMapper();
		JsonNode jsonResult = om.readTree(resultBody);

		long consultedId = jsonResult.findValue("id").asLong();
		String consultedIdentifiant = jsonResult.findValue("identifiant").toString();
		
		Assertions.assertThat(responseSchema.validate(jsonResult).isSuccess()).isTrue();
		Assertions.assertThat(consultedIdentifiant).isEqualTo(data.get("identifiant").asText());
		
		//---------------------------------------------------------------------------------------
		
		final JsonNode updateData = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-update.json");
		final String updateInputJson = updateData.toString();
		
		consultedOA.setStatut((updateData.get("statut").asText()));
		Mockito.when(service.save(Mockito.any(OperationArcheologique.class))).thenReturn(consultedOA);
		
		RequestBuilder uprb = MockMvcRequestBuilders.put(uri).accept(MediaType.APPLICATION_JSON)
				.content(consultedOA.toString())
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult updateResult = mockMvc.perform(uprb).andExpect(status().isOk()).andReturn();
		MockHttpServletResponse updateResponse = updateResult.getResponse();
		String updateResultBody = updateResponse.getContentAsString();
		

		final JsonNode updateResFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-res-update.json");
		final JsonSchema updateResponseSchema = factory.getJsonSchema(updateResFileSchema);
		ObjectMapper upom = new ObjectMapper();
		System.out.println(updateResultBody);

		JsonNode updateJsonResult = upom.readTree(updateResultBody);
		
		String upconsultedStatut = updateJsonResult.findValue("statut").toString();

		Assertions.assertThat(updateResponseSchema.validate(updateJsonResult).isSuccess()).isTrue();
		
	}

	@Test
	public void updateChantier_notFound() throws Exception {
		
		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-update.json");
		final String inputJson = data.toString();
		
		final Long testId = 18L;
		final String uri = "/chantiers/" + testId;
		
		OperationArcheologique consultedOA = new OperationArcheologique();
		consultedOA.setIdentifiant(data.get("identifiant").asText());
		consultedOA.setDesignationDecree(data.get("designationDecree").asText());
		consultedOA.setPrescriptionDecree(data.get("prescriptionDecree").asText());
		consultedOA.setId(testId);
		Date date = new Date();
		consultedOA.setCreatedDate(date);
		consultedOA.setLastModifiedDate(date);


		Mockito.when(service.save(Mockito.any(OperationArcheologique.class))).thenThrow(ResourceNotFoundException.class);

		RequestBuilder rb = MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON).content(consultedOA.toString());

		MvcResult result = mockMvc.perform(rb).andExpect(status().isNotFound()).andReturn();
		MockHttpServletResponse response = result.getResponse();

		Assertions.assertThat(response.getContentAsString().isEmpty());

	}
	
	@Test
	public void updateChantier_badRequest() throws Exception {
		
		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-update.json");
		final String inputJson = data.toString();
		
		final Long testId = 18L;
		final String uri = "/chantiers/" + testId;
		
		OperationArcheologique consultedOA = new OperationArcheologique();
		consultedOA.setIdentifiant(data.get("identifiant").asText());
		consultedOA.setDesignationDecree(data.get("designationDecree").asText());
		consultedOA.setPrescriptionDecree(data.get("prescriptionDecree").asText());
		consultedOA.setId(testId);
		Date date = new Date();
		consultedOA.setCreatedDate(date);
		consultedOA.setLastModifiedDate(date);

		Mockito.when(service.save(Mockito.any(OperationArcheologique.class))).thenThrow(NotValidObjectException.class);

		RequestBuilder rb = MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON).content(consultedOA.toString());

		MvcResult result = mockMvc.perform(rb).andExpect(status().isBadRequest()).andReturn();
		MockHttpServletResponse response = result.getResponse();

		Assertions.assertThat(response.getContentAsString().isEmpty());

	}
}
