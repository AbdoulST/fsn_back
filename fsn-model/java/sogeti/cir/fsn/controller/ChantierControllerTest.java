package sogeti.cir.fsn.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.model.neo4j.Chantier;
import sogeti.cir.fsn.service.ChantierService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ChantierControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ChantierService service;

	final String RESOURCE_PATH = "src/test/resources/schemas/chantier";
	final Path REAL_PATH = Paths.get(RESOURCE_PATH);
	final Long UNKNOWN_ID = Long.MAX_VALUE;
	final String URL_PREFIX = "/chantier";

	@Test
	public void createChantier_success() throws Exception {
		// #### validation du JSON envoyé ####
		
		// récupération du JSON d'envois
		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-create.json");

		// récupération du JSON Schema 
		final JsonNode reqFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-req-create.json");
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final JsonSchema requestSchema = factory.getJsonSchema(reqFileSchema);
		
		// validation
		Assertions.assertThat(requestSchema.validate(data).isSuccess()).isTrue();

		// Mock du service
		Chantier createdChantier = new Chantier();
		createdChantier.setFullName(data.get("fullName").asText());
		createdChantier.setShortName(data.get("shortName").asText());
		createdChantier.setChantierCode(data.get("chantierCode").asText());
		createdChantier.setDescription(data.get("description").asText());
		Date date = new Date();
		createdChantier.setCreatedDate(date);
		createdChantier.setLastModifiedDate(date);
		createdChantier.setEntryAuthor("Carine");
		Mockito.when(service.save(Mockito.any(Chantier.class))).thenReturn(createdChantier);

		// #### request ####
		final String inputJson = data.toString();
		RequestBuilder rb = MockMvcRequestBuilders.post(URL_PREFIX).accept(MediaType.APPLICATION_JSON).content(inputJson)
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(rb).andExpect(status().isCreated()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();

		// #### vérification de la response
		final JsonNode resFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-res-create.json");
		final JsonSchema responseSchema = factory.getJsonSchema(resFileSchema);
		ObjectMapper om = new ObjectMapper();
		System.out.println(resultBody);
		JsonNode jsonResult = om.readTree(resultBody);
		Assertions.assertThat(responseSchema.validate(jsonResult).isSuccess()).isTrue();

	}

	@Test
	public void create_empty_chantier_fail() throws Exception {
		RequestBuilder rb = MockMvcRequestBuilders.post(URL_PREFIX).contentType(MediaType.APPLICATION_JSON);
		mockMvc.perform(rb).andExpect(status().isBadRequest()).andReturn();
	}

	@Test
	public void findChantier_byId_success() throws Exception {

		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-create.json");
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final Long testId = 18L;
		final String uri = "/chantiers/" + testId;

		Chantier consultedChantier = new Chantier();
		consultedChantier.setFullName(data.get("fullName").asText());
		consultedChantier.setShortName(data.get("shortName").asText());
		consultedChantier.setChantierCode(data.get("chantierCode").asText());
		consultedChantier.setDescription(data.get("description").asText());
		consultedChantier.setId(testId);
		Date date = new Date();
		consultedChantier.setCreatedDate(date);
		consultedChantier.setLastModifiedDate(date);
		consultedChantier.setEntryAuthor("Carine");

		Mockito.when(service.findOne(Mockito.eq(testId))).thenReturn(consultedChantier);

		RequestBuilder rb = MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isOk()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();

		final JsonNode resFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-res-read.json");
		final JsonSchema responseSchema = factory.getJsonSchema(resFileSchema);

		ObjectMapper om = new ObjectMapper();
		JsonNode jsonResult = om.readTree(resultBody);

		long consultedId = jsonResult.findValue("id").asLong();

		Assertions.assertThat(responseSchema.validate(jsonResult).isSuccess()).isTrue();
		Assertions.assertThat(consultedId).isEqualTo(testId);

	}

	@Test
	public void findChantier_byId_fail() throws Exception {

		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-create.json");
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final Long testBadId = 25L;
		final String uri = "/chantiers/" + testBadId;

		Mockito.when(service.findOne(Mockito.eq(testBadId))).thenThrow(ResourceNotFoundException.class);

		RequestBuilder rb = MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isNotFound()).andReturn();

		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();

		Assertions.assertThat(resultBody).isNullOrEmpty();

	}

	 @Test
	 public void findAllChantier_success() throws Exception {
		
		// mock des data
		Date date = new Date();
		
		Chantier chantier1 = new Chantier();
		chantier1.setFullName("First Chantier Test");
		chantier1.setShortName("First");
		chantier1.setChantierCode("ch1");
		chantier1.setDescription("description 1");
		chantier1.setCreatedDate(date);
		chantier1.setLastModifiedDate(date);
		chantier1.setEntryAuthor("Carine");
		chantier1.setId(10L);
		
		Chantier chantier2 = new Chantier();
		chantier2.setFullName("Second Chantier Test");
		chantier2.setShortName("Second");
		chantier2.setChantierCode("ch2");
		chantier2.setDescription("description 2");
		chantier2.setCreatedDate(date);
		chantier2.setLastModifiedDate(date);
		chantier2.setEntryAuthor("Carine");
		chantier2.setId(19L);
		 
		Pageable page = null;
		List<Chantier> consultedChantier = new ArrayList<>();
		consultedChantier.add(chantier1);
		consultedChantier.add(chantier2);
		Page<Chantier> pageChantier = new PageImpl<Chantier>(consultedChantier);
		Mockito.when(service.findAllByPage(page)).thenReturn(pageChantier);
		 
		// request
		final String uri = "/chantiers";
		RequestBuilder rb = MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(rb).andExpect(status().isOk()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();
		
		// vérification de la response
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final JsonNode resFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath()+"/schema-res-readAll.json");
		final JsonSchema responseSchema = factory.getJsonSchema(resFileSchema);
		ObjectMapper om = new ObjectMapper();
		JsonNode jsonResult = om.readTree(resultBody);
		Assertions.assertThat(responseSchema.validate(jsonResult).isSuccess()).isTrue();
	
	 }

	@Test
	public void deleteChantier_success() throws Exception {

		final Long testId = 18L;
		final String uri = "/chantiers/" + testId;

		Mockito.when(service.delete(Mockito.eq(testId))).thenReturn(true);

		RequestBuilder rb = MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isNoContent()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBodyDel = response.getContentAsString();
		
		Assertions.assertThat(resultBodyDel).isNullOrEmpty();

	}
	
	@Test
	public void deleteChantier_notFound() throws Exception {

		final Long testId = 18L;
		final String uri = "/chantiers/" + testId;

		Mockito.when(service.delete(Mockito.eq(testId))).thenReturn(false);

		RequestBuilder rb = MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isNoContent()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBodyDel = response.getContentAsString();
		
		Assertions.assertThat(resultBodyDel).isNullOrEmpty();

	}
	
	@Test
	public void updateChantier_success() throws Exception {

		// #### validation du JSON envoyé ####
		
		// récupération du JSON d'envois
		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-update.json");
	
		// récupération du JSON Schema 
		final JsonNode reqFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-req-update.json");
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		final JsonSchema requestSchema = factory.getJsonSchema(reqFileSchema);
		
		// validation
		Assertions.assertThat(requestSchema.validate(data).isSuccess()).isTrue();
	
		// Mock du service
		Chantier updatedChantier = new Chantier();
		updatedChantier.setFullName(data.get("fullName").asText());
		updatedChantier.setShortName(data.get("shortName").asText());
		updatedChantier.setChantierCode(data.get("chantierCode").asText());
		updatedChantier.setDescription(data.get("description").asText());
		Date date = new Date();
		updatedChantier.setCreatedDate(date);
		updatedChantier.setLastModifiedDate(date);
		updatedChantier.setEntryAuthor("Fluffy");
		updatedChantier.setId(data.get("id").asLong());
		Mockito.when(service.save(Mockito.any(Chantier.class))).thenReturn(updatedChantier);

		RequestBuilder uprb = MockMvcRequestBuilders.put(URL_PREFIX+"/"+updatedChantier.getId()).accept(MediaType.APPLICATION_JSON)
				.content(data.toString())
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult updateResult = mockMvc.perform(uprb).andExpect(status().isOk()).andReturn();
		MockHttpServletResponse updateResponse = updateResult.getResponse();
		String updateResultBody = updateResponse.getContentAsString();

		final JsonNode updateResFileSchema = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/schema-res-update.json");
		final JsonSchema updateResponseSchema = factory.getJsonSchema(updateResFileSchema);
		ObjectMapper upom = new ObjectMapper();
		System.out.println(updateResultBody);

		JsonNode updateJsonResult = upom.readTree(updateResultBody);
		
		String upconsultedEntryAuthor = updateJsonResult.findValue("entryAuthor").toString();

		Assertions.assertThat(updateResponseSchema.validate(updateJsonResult).isSuccess()).isTrue();
		
	}

	@Test
	public void updateChantier_notFound() throws Exception {
		
		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-update.json");
		final String inputJson = data.toString();
		
		final Long testId = 18L;
		final String uri = "/chantiers/" + testId;
		
		Chantier consultedChantier = new Chantier();
		consultedChantier.setFullName(data.get("fullName").asText());
		consultedChantier.setShortName(data.get("shortName").asText());
		consultedChantier.setChantierCode(data.get("chantierCode").asText());
		consultedChantier.setDescription(data.get("description").asText());
		consultedChantier.setId(testId);
		Date date = new Date();
		consultedChantier.setCreatedDate(date);
		consultedChantier.setLastModifiedDate(date);
		consultedChantier.setEntryAuthor("Fluffy");

		Mockito.when(service.save(Mockito.any(Chantier.class))).thenThrow(ResourceNotFoundException.class);

		RequestBuilder rb = MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON).content(consultedChantier.toString());

		MvcResult result = mockMvc.perform(rb).andExpect(status().isNotFound()).andReturn();
		MockHttpServletResponse response = result.getResponse();

		Assertions.assertThat(response.getContentAsString().isEmpty());

	}
	
	@Test
	public void updateChantier_badRequest() throws Exception {
		
		final JsonNode data = JsonLoader.fromPath(REAL_PATH.toRealPath() + "/req-update-bad.json");
		final String inputJson = data.toString();
		
		final Long testId = 18L;
		final String uri = "/chantiers/" + testId;
		
		Chantier consultedChantier = new Chantier();
		consultedChantier.setFullName(data.get("fullName").asText());
		consultedChantier.setChantierCode(data.get("chantierCode").asText());
		consultedChantier.setId(testId);
		Date date = new Date();
		consultedChantier.setCreatedDate(date);
		consultedChantier.setLastModifiedDate(date);
		consultedChantier.setEntryAuthor("Fluffy");

		Mockito.when(service.save(Mockito.any(Chantier.class))).thenThrow(NotValidObjectException.class);

		RequestBuilder rb = MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON).content(consultedChantier.toString());

		MvcResult result = mockMvc.perform(rb).andExpect(status().isBadRequest()).andReturn();
		MockHttpServletResponse response = result.getResponse();

		Assertions.assertThat(response.getContentAsString().isEmpty());

	}

}
