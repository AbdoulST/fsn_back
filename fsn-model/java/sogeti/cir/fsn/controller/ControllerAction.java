package sogeti.cir.fsn.controller;

public enum ControllerAction {
	CREATE("create"),
	REMOVE("remove"),
	UPDATE("update"),
	DISPLAY("display");
	
	private String name = "";
	   
	ControllerAction(String name){
	    this.name = name;
	}
	   
	public String toString(){
		return name;
	}
			
}
