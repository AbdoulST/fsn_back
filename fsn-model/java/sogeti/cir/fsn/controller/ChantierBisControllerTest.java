package sogeti.cir.fsn.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.JsonNode;

import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.model.neo4j.Chantier;
import sogeti.cir.fsn.service.ChantierService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ChantierBisControllerTest extends AbstractControllerTest<Chantier>{

	@MockBean
	private ChantierService service;

	public ChantierBisControllerTest() {
		super(Chantier.NAME.toLowerCase()+"s", Chantier.class);
	}
	
	@Override
	protected AbstractService<Chantier> getService() {
		return this.service;
	}

	@Test
	public void create_success() throws Exception {
		JsonNode data = this.getJsonRessouce(ControllerAction.CREATE, true);
		Chantier createdChantier = new Chantier();
		createdChantier.setFullName(data.get("fullName").asText());
		createdChantier.setShortName(data.get("shortName").asText());
		createdChantier.setChantierCode(data.get("chantierCode").asText());
		createdChantier.setDescription(data.get("description").asText());
		Date date = new Date();
		createdChantier.setCreatedDate(date);
		createdChantier.setLastModifiedDate(date);
		createdChantier.setEntryAuthor("Carine");
		
		this.createSuccess(createdChantier);
	}
	
	@Test
	public void create_no_description_fail() throws IOException, Exception {
		JsonNode data = this.getJsonRessouce(ControllerAction.CREATE, true, "-no-description");
		createFail(data, false, NotValidObjectException.class, status().isBadRequest());
	}
	
	@Test
	public void display_one_success() throws Exception {
		JsonNode data = this.getJsonRessouce(ControllerAction.CREATE, true);
		Chantier consultedChantier = new Chantier();
		consultedChantier.setFullName(data.get("fullName").asText());
		consultedChantier.setShortName(data.get("shortName").asText());
		consultedChantier.setChantierCode(data.get("chantierCode").asText());
		consultedChantier.setDescription(data.get("description").asText());
		Date date = new Date();
		consultedChantier.setCreatedDate(date);
		consultedChantier.setLastModifiedDate(date);
		consultedChantier.setEntryAuthor("Carine");
		consultedChantier.setId(1L);
		
		this.findOneSuccess(consultedChantier);
	}

}
