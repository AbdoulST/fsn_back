package sogeti.cir.fsn.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.JsonParser.NumberType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.model.neo4j.NeoModel;

public abstract class AbstractControllerTest<T extends NeoModel> {
	
	protected final String RESOURCES_PATH = "src/test/resources/schemas";
	protected final String URL_PREFIX;
	protected final String RESOURCE_NAME;
	protected final String RESOURCES_OBJ_PATH;
	protected final Path ABSOLUTE_REOURCE_OBJ_PATH;
	protected final Class<T> OBJ_TYPE;
	
	@Autowired
	private MockMvc mockMvc;
	
	protected AbstractControllerTest(String resourceName, Class<T> type) {
		this.RESOURCE_NAME = resourceName;
		this.URL_PREFIX = "/"+this.RESOURCE_NAME;
		this.RESOURCES_OBJ_PATH = RESOURCES_PATH + "/" + this.RESOURCE_NAME;
		this.ABSOLUTE_REOURCE_OBJ_PATH = Paths.get(RESOURCES_OBJ_PATH);
		this.OBJ_TYPE = type;
	}

	protected abstract AbstractService<T> getService();
	
	protected String getJSONFilePath(boolean isRequest, ControllerAction ca, boolean isSchema, String suffix) throws IOException {
		return ABSOLUTE_REOURCE_OBJ_PATH.toRealPath() + "/"
			+ ca + "-"
			+ (isRequest ? "req" : "res")
			+ (isSchema ? "-schema" : "") + suffix +".json";
	}
	
	protected JsonNode getJsonRessouce(ControllerAction ca, boolean isRequest) throws IOException {
		return this.getJsonRessouce(ca, isRequest, false, "");
	}

	protected JsonNode getJsonRessouce(ControllerAction ca, boolean isRequest, String suffix) throws IOException {
		return getJsonRessouce(ca, isRequest, false, suffix);
	}
	
	private JsonNode getJsonRessouce(ControllerAction ca, boolean isRequest, boolean isSchema, String suffix) throws IOException {
		return JsonLoader.fromPath(getJSONFilePath(isRequest, ca, isSchema, suffix));
	}
	
	
	protected JsonSchema getJsonSchema(ControllerAction ca, boolean isRequest) throws IOException, ProcessingException {
		return this.getJsonSchema(ca, isRequest, "");
	}
	
	protected JsonSchema getJsonSchema(ControllerAction ca, boolean isRequest, String suffix) throws IOException, ProcessingException {
		final JsonNode reqFileSchema = getJsonRessouce(ca, isRequest, true, suffix);
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		return factory.getJsonSchema(reqFileSchema);
	}
	
	protected JsonNode getJsonNodeResult(MvcResult result) throws IOException{
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();
		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		ObjectMapper om = new ObjectMapper();
		return om.readTree(resultBody);
	}
	
	public void createSuccess(T createdObj) throws Exception {
		
		ControllerAction ca = ControllerAction.CREATE;
		final JsonNode data = getJsonRessouce(ca, true);
		final JsonSchema requestSchema = getJsonSchema(ca,true);
		Assertions.assertThat(requestSchema.validate(data).isSuccess()).isTrue();
		
		Mockito.when(this.getService().save(Mockito.any(OBJ_TYPE))).thenReturn(createdObj);

		// #### request ####
		final String inputJson = data.toString();
		RequestBuilder rb = MockMvcRequestBuilders.post(URL_PREFIX).accept(MediaType.APPLICATION_JSON).content(inputJson)
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(rb).andExpect(status().isCreated()).andReturn();
		MockHttpServletResponse response = result.getResponse();
		String resultBody = response.getContentAsString();

		// #### vérification de la response
		final JsonSchema responseSchema = getJsonSchema(ca, false);
		ObjectMapper om = new ObjectMapper();
		JsonNode jsonResult = om.readTree(resultBody);
		Assertions.assertThat(responseSchema.validate(jsonResult).isSuccess()).isTrue();

	}
	
	protected void createFail(JsonNode data, boolean validShema, Class<? extends Throwable> serviceException, ResultMatcher status) throws Exception {
		
		ControllerAction ca = ControllerAction.CREATE;
		final JsonSchema requestSchema = getJsonSchema(ca,true);
		
		if(data != null) {
			if(validShema)
				Assertions.assertThat(requestSchema.validate(data).isSuccess()).isTrue();
			else
				Assertions.assertThat(requestSchema.validate(data).isSuccess()).isFalse();
		}
		
		if(serviceException != null)
			Mockito.when(this.getService().save(Mockito.any(OBJ_TYPE))).thenThrow(serviceException);

		// #### request ####
		final String inputJson = data == null ? "" : data.toString();
		RequestBuilder rb = MockMvcRequestBuilders.post(URL_PREFIX).accept(MediaType.APPLICATION_JSON).content(inputJson)
				.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(rb).andExpect(status).andReturn();

	}
	
	@Test
	public void create_empty_input_fail() throws Exception {
		createFail(null, false, null, status().isBadRequest()); 
	}
	
	public void findOneSuccess(T returnedObj) throws Exception {
		Mockito.when(getService().findOne(Mockito.eq(returnedObj.getId()))).thenReturn(returnedObj);

		RequestBuilder rb = MockMvcRequestBuilders.get(URL_PREFIX + "/" + returnedObj.getId())
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status().isOk()).andReturn();
		JsonNode jsonResult = getJsonNodeResult(result);

		long consultedId = jsonResult.findValue("id").asLong();
		Assertions.assertThat(consultedId).isEqualTo(returnedObj.getId());
		JsonSchema responseSchema = getJsonSchema(ControllerAction.DISPLAY, false);
		Assertions.assertThat(responseSchema.validate(jsonResult).isSuccess()).isTrue();
	}
	
	public void findOneFail(Class<? extends Throwable> serviceException, ResultMatcher status) throws Exception{
		Long consultedId = 1L;
		Mockito.when(getService().findOne(Mockito.eq(consultedId))).thenThrow(serviceException);

		RequestBuilder rb = MockMvcRequestBuilders.get(URL_PREFIX + "/" + consultedId)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(rb).andExpect(status).andReturn();
	}
	
	@Test
	public void find_not_existing_ressource_fail() throws Exception {
		this.findOneFail(ResourceNotFoundException.class, status().isNotFound());
	}
	

}
