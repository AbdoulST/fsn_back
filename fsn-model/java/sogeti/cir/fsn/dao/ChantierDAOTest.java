//package sogeti.cir.fsn.dao;
//
//import java.util.Date;
//
//import org.assertj.core.api.Assertions;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.neo4j.ogm.session.Session;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import sogeti.cir.fsn.dao.ChantierDAO;
//import sogeti.cir.fsn.model.neo4j.Chantier;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@Transactional
//public class ChantierDAOTest {
//
//	@Autowired
//	private Session session;
//
//	@Autowired
//	private ChantierDAO chantierDAO;
//
//	@Before
//	public void setUp() {
//		chantierDAO.deleteAll();
//	}
//	
//	@Test
//	public void test_when_saveChantier() {
//		Chantier chantier = new Chantier();
//		Date date = new Date();
//		chantier.setYear(date);
//		chantier.setCadastralYear(date);
//		chantier.setEntryAuthor("Robin");
//		chantier.setAddress("Chaville");
//		chantier.setCreatedDate(date);
//		chantier.setLastModifiedDate(date);
//		chantier.setDateEntry(date);
//		chantier.setDescription("Le chantier toto");
//		chantier.setLastModifiedDate(date);
//		chantier.setFullName("Toto");
//		chantier.setShortName("TT");
//		chantier.setChantierCode("118712");
//		chantier.setInseeCode("118218");
//		chantier.setProtege(true);
//		chantier.setVersion(1792);
//		chantier.setXCentroide(12358132134L);
//		chantier.setXMax(34211385321L);
//		chantier.setYCentroide(-12358132134L);
//		chantier.setYMax(-34211385321L);
//		
//		Chantier newChantier = chantierDAO.save(chantier);
//		Assertions.assertThat(chantier.getId()).isNotNull();
//	}
//}