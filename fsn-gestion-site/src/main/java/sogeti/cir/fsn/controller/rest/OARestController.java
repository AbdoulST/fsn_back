package sogeti.cir.fsn.controller.rest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.model.OperationArcheologique;

@RestController
@RequestMapping("/oa")
public class OARestController extends AbstractRestController<OperationArcheologique> {

	public OARestController(AbstractService<OperationArcheologique> service) {
		super(service);
		
	}

}
