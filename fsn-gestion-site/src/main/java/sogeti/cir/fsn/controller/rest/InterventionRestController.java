package sogeti.cir.fsn.controller.rest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.model.Intervention;

@RestController
@RequestMapping("/interventions")
//@PreAuthorize("hasRole('ROLE_ADMIN')")
public class InterventionRestController extends AbstractRestController<Intervention> {

	public InterventionRestController(AbstractService<Intervention> service) {
		super(service);

	}

}
