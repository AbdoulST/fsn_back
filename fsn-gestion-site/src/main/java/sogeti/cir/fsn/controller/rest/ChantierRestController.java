package sogeti.cir.fsn.controller.rest;

import javax.websocket.server.PathParam;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sogeti.cir.fsn.api.controller.rest.AbstractRestController;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.model.Chantier;

@RestController
@RequestMapping("/chantiers")
//@PreAuthorize("hasRole('ROLE_USER')")
public class ChantierRestController extends AbstractRestController<Chantier> {

	public ChantierRestController(AbstractService<Chantier> service) {
		super(service);

	}

}
