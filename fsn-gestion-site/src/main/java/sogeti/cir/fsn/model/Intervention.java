package sogeti.cir.fsn.model;

import java.util.Date;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Component;

import sogeti.cir.fsn.model.neo4j.NeoModel;

@Component
@NodeEntity(label = Intervention.NAME)
public class Intervention extends NeoModel {

	public static final String NAME = "intervention";

	@NotNull
	private Integer interventionNumber;

	@Relationship(type = "CONCERNED", direction = Relationship.OUTGOING)
	private Chantier chantier;

	@Relationship(type = "ASSOCIATED", direction = Relationship.INCOMING)
	public Set<Intervention> associatedInterventions;

	@Length(min = 4, max = 4)
	private String yearIntervention;

	private String authorSeizure;
	private String categoryIntervention;

	@DateTimeFormat(pattern = "dd/MM/yyyy", iso = ISO.DATE)
	private Date startDateIntervention;

	@DateTimeFormat(pattern = "dd/MM/yyyy", iso = ISO.DATE)
	private Date endDateIntervention;

	private String legalStatusOfTheSearch;

	private String comment;

	public String getLabel() {
		return null;
	}

	public String getName() {
		return null;
	}

	/**
	 * @return the interventionNumber
	 */
	public Integer getInterventionNumber() {
		return interventionNumber;
	}

	/**
	 * @return the chantierCode
	 */
	public Chantier getChantier() {
		return chantier;
	}

	/**
	 * @return the yearIntervention
	 */
	public String getYearIntervention() {
		return yearIntervention;
	}

	/**
	 * @return the authorSeizure
	 */
	public String getAuthorSeizure() {
		return authorSeizure;
	}

	/**
	 * @return the categoryIntervention
	 */
	public String getCategoryIntervention() {
		return categoryIntervention;
	}

	/**
	 * @return the startDateIntervention
	 */
	public Date getStartDateIntervention() {
		return startDateIntervention;
	}

	/**
	 * @return the endDateIntervention
	 */
	public Date getEndDateIntervention() {
		return endDateIntervention;
	}

	/**
	 * @return the legalStatusOfTheSearch
	 */
	public String getLegalStatusOfTheSearch() {
		return legalStatusOfTheSearch;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param interventionNumber
	 *            the interventionNumber to set
	 */
	public void setInterventionNumber(Integer interventionNumber) {
		this.interventionNumber = interventionNumber;
	}

	/**
	 * @param chantierCode
	 *            the chantierCode to set
	 */
	public void setChantier(Chantier chantier) {
		this.chantier = chantier;
	}

	/**
	 * @param yearIntervention
	 *            the yearIntervention to set
	 */
	public void setYearIntervention(String yearIntervention) {
		this.yearIntervention = yearIntervention;
	}

	/**
	 * @param authorSeizure
	 *            the authorSeizure to set
	 */
	public void setAuthorSeizure(String authorSeizure) {
		this.authorSeizure = authorSeizure;
	}

	/**
	 * @param categoryIntervention
	 *            the categoryIntervention to set
	 */
	public void setCategoryIntervention(String categoryIntervention) {
		this.categoryIntervention = categoryIntervention;
	}

	/**
	 * @param startDateIntervention
	 *            the startDateIntervention to set
	 */
	public void setStartDateIntervention(Date startDateIntervention) {
		this.startDateIntervention = startDateIntervention;
	}

	/**
	 * @param endDateIntervention
	 *            the endDateIntervention to set
	 */
	public void setEndDateIntervention(Date endDateIntervention) {
		this.endDateIntervention = endDateIntervention;
	}

	/**
	 * @param legalStatusOfTheSearch
	 *            the legalStatusOfTheSearch to set
	 */
	public void setLegalStatusOfTheSearch(String legalStatusOfTheSearch) {
		this.legalStatusOfTheSearch = legalStatusOfTheSearch;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the associatedInterventions
	 */
	public Set<Intervention> getAssociatedInterventions() {
		return associatedInterventions;
	}

	/**
	 * @param associatedInterventions
	 *            the associatedInterventions to set
	 */
	public void setAssociatedInterventions(Set<Intervention> associatedInterventions) {
		this.associatedInterventions = associatedInterventions;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Intervention other = (Intervention) obj;
		if (associatedInterventions == null) {
			if (other.associatedInterventions != null)
				return false;
		} else if (!associatedInterventions.equals(other.associatedInterventions))
			return false;
		if (authorSeizure == null) {
			if (other.authorSeizure != null)
				return false;
		} else if (!authorSeizure.equals(other.authorSeizure))
			return false;
		if (categoryIntervention == null) {
			if (other.categoryIntervention != null)
				return false;
		} else if (!categoryIntervention.equals(other.categoryIntervention))
			return false;
		if (chantier == null) {
			if (other.chantier != null)
				return false;
		} else if (!chantier.equals(other.chantier))
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (endDateIntervention == null) {
			if (other.endDateIntervention != null)
				return false;
		} else if (!endDateIntervention.equals(other.endDateIntervention))
			return false;
		if (interventionNumber == null) {
			if (other.interventionNumber != null)
				return false;
		} else if (!interventionNumber.equals(other.interventionNumber))
			return false;
		if (legalStatusOfTheSearch == null) {
			if (other.legalStatusOfTheSearch != null)
				return false;
		} else if (!legalStatusOfTheSearch.equals(other.legalStatusOfTheSearch))
			return false;
		if (startDateIntervention == null) {
			if (other.startDateIntervention != null)
				return false;
		} else if (!startDateIntervention.equals(other.startDateIntervention))
			return false;
		if (yearIntervention == null) {
			if (other.yearIntervention != null)
				return false;
		} else if (!yearIntervention.equals(other.yearIntervention))
			return false;
		return true;
	}

}
