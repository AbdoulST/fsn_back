package sogeti.cir.fsn.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import sogeti.cir.fsn.model.neo4j.NeoModel;

@Component
@NodeEntity
public class OperationArcheologique extends NeoModel {

	private static final long serialVersionUID = 1L;
	
	public static final String NAME ="oa";

	@NotNull
	@NotBlank
	@Length(min = 0, max = 50)
	private String identifiant;
	
	@Relationship(type = "CONCERNED", direction = Relationship.OUTGOING)
	private Chantier chantier;

	private String designationDecree;
	
	private String prescriptionDecree;

	@DateTimeFormat(pattern = "yyyy-MM-dd", iso = ISO.DATE)
	private Date startDate;

	@DateTimeFormat(pattern = "yyyy-MM-dd", iso = ISO.DATE)
	private Date endDate;
	
	private String statut;
	
	private String nature;
	
	private String excavationCondition;
	
	private String rattachmentOrganism;
	
	private String maitriseOuvrage;
	
	private Long surface;
	
	private String comment;
	
	@Override
	public String getLabel() {
		return getIdentifiant();
	}

	@Override
	public String getName() {
		return NAME;
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	
	public Chantier getChantier() {
		return chantier;
	}

	public void setChantier(Chantier chantier) {
		this.chantier = chantier;
	}

	public String getDesignationDecree() {
		return designationDecree;
	}

	public void setDesignationDecree(String designationDecree) {
		this.designationDecree = designationDecree;
	}

	public String getPrescriptionDecree() {
		return prescriptionDecree;
	}

	public void setPrescriptionDecree(String prescriptionDecree) {
		this.prescriptionDecree = prescriptionDecree;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getExcavationCondition() {
		return excavationCondition;
	}

	public void setExcavationCondition(String excavationCondition) {
		this.excavationCondition = excavationCondition;
	}

	public String getRattachmentOrganism() {
		return rattachmentOrganism;
	}

	public void setRattachmentOrganism(String rattachmentOrganism) {
		this.rattachmentOrganism = rattachmentOrganism;
	}

	public String getMaitriseOuvrage() {
		return maitriseOuvrage;
	}

	public void setMaitriseOuvrage(String maitriseOuvrage) {
		this.maitriseOuvrage = maitriseOuvrage;
	}

	public Long getSurface() {
		return surface;
	}

	public void setSurface(Long surface) {
		this.surface = surface;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public OperationArcheologique() {
		super();
	}
	
	

}
