package sogeti.cir.fsn.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.DescriptiveObjectError;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.dao.ChantierDAO;
import sogeti.cir.fsn.dao.InterventionDAO;
import sogeti.cir.fsn.model.Chantier;
import sogeti.cir.fsn.model.Intervention;

@Service
@Transactional
public class InterventionService extends AbstractService<Intervention> {

	@Autowired
	ChantierDAO chantierDao;

	@Autowired
	InterventionDAO interventionDAO;

	public InterventionService(AbstractDao<Intervention> dao) {
		super(dao);

	}
	

	@Override
	protected void additionalSaveChecks(Intervention objet, DescriptiveObjectError doe) {
		verifChantier(objet, doe);
		verifInterventions(objet, doe);
	}

	/**
	 * verification à l'acte de la creation, si l'Id du chantier de l'intervention
	 * passé en parametre est lié à un chantier present dans la BD, et si ce dernier
	 * existe on recupere toutes les données de façon à ce que se ne soit pas ecrasé
	 * 
	 * @param Intervention
	 * 
	 */
	public void verifChantier(Intervention objet, DescriptiveObjectError doe) {
		Assert.notNull(objet, "Ne peut pas être null.");
		if (objet.getChantier() != null) {
			Long id = objet.getChantier().getId();
			Optional<Chantier> c = chantierDao.findById(id);
			if (c.isPresent())
				objet.setChantier(c.get());
		}
	}

	/**
	 * 
	 * @param objet
	 *            verification à l'acte de la creation, si est lié à une ou même à
	 *            plusieurs interventions , et si ces derniers existent en BD
	 */
	public void verifInterventions(Intervention objet, DescriptiveObjectError doe) {
		if (objet.getAssociatedInterventions() != null) {
			Set<Intervention> associatedInterventions = new HashSet<>();
			for (Intervention item : objet.getAssociatedInterventions()) {
				Optional<Intervention> i = interventionDAO.findById(item.getId());
				if (i.isPresent()) {
					associatedInterventions.add(i.get());
				}
				objet.setAssociatedInterventions(associatedInterventions);
			}
		}
	}

}
