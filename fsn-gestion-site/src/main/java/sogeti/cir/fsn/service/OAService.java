package sogeti.cir.fsn.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.DescriptiveObjectError;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.dao.ChantierDAO;
import sogeti.cir.fsn.dao.OADAO;
import sogeti.cir.fsn.model.Chantier;
import sogeti.cir.fsn.model.OperationArcheologique;

// cette classe assure les services associés à l'opération archéologique, mais elle hérite de la classe
// AbstractService Qui est un service générique pour tt le projet
@Service
@Transactional
public class OAService extends AbstractService<OperationArcheologique> {
	
	@Autowired
	ChantierDAO chantierDao;

	@Autowired
	OADAO oaDAO;

	public OAService(AbstractDao<OperationArcheologique> dao) {
		super(dao);
		
	}
	
	

	@Override
	protected void additionalSaveChecks(OperationArcheologique objet, DescriptiveObjectError doe) {
		verifChantier(objet,doe);
	}

	/**
	 * verification à l'acte de la creation, si l'Id du chantier de l'interventions
	 * passé en parametre est lié à un chantier present dans la BD, et si ce dernier
	 * existe on recupere tous les donnés de facon que ne soi pas ecrasé
	 * 
	 * @param Intervention
	 * @throws NotValidObjectException 
	 * 
	 */
	public void verifChantier(OperationArcheologique objet,
			DescriptiveObjectError doe) {
		Assert.notNull(objet, "Ne peut pas être null.");
		if (objet.getChantier() != null) {
			Long id = objet.getChantier().getId();
			Optional<Chantier> c = chantierDao.findById(id);
			if (c.isPresent()) 
				objet.setChantier(c.get());
			else 
				doe.addError("chantier", "Ce chantier n'existe pas");
		}
	}	
	
	public static boolean isOverlapping(Date start1, Date end1, Date start2, Date end2) {
		boolean overlap = false;
		if (start1.equals(start2) || end1.equals(end2)) {
		    overlap = true;
			}
		
		if ((start1.after(start2) && start1.before(end2))||(end1.before(end2) && end1.after(start2))) {
	        overlap = true;
		}
		return overlap;
	}
	
}
