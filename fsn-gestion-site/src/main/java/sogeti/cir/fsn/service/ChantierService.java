package sogeti.cir.fsn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.FsnServiceException;
import sogeti.cir.fsn.api.neo4j.service.IllegalOperationException;
import sogeti.cir.fsn.api.neo4j.service.ResourceNotFoundException;
import sogeti.cir.fsn.dao.ChantierDAO;
import sogeti.cir.fsn.dao.OADAO;
import sogeti.cir.fsn.model.Chantier;
import java.util.Optional;

// cett eclasse assure les services associés au chantier, mais elle hérite de la classe
// AbstractService Qui est un service générique pour tt le projet
@Service
@Transactional
public class ChantierService extends AbstractService<Chantier> {
	
	@Autowired
	ChantierDAO chantierDao;
	
	
	public ChantierService(AbstractDao<Chantier> dao) {
		super(dao);	
	}
	
	

}
