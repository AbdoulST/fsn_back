package sogeti.cir.fsn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@EnableDiscoveryClient
@SpringBootApplication
@EntityScan("sogeti.cir.fsn.model")
public class FsnGestionSiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(FsnGestionSiteApplication.class, args);
	}
}
