package sogeti.cir.fsn.dao;


import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.query.Param;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.OperationArcheologique;

public interface OADAO extends AbstractDao<OperationArcheologique> {
	
	@Query("MATCH (o:OperationArcheologique)-->(c:Chantier) " + "WHERE id(c) = {id}" + "RETURN o")
	public List<OperationArcheologique> getList(@Param("id") Long id);
}


