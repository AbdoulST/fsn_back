package sogeti.cir.fsn.dao;


import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.Chantier;

public interface ChantierDAO extends AbstractDao<Chantier> {

}
