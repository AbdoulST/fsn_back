package sogeti.cir.fsn.dao;

import sogeti.cir.fsn.api.neo4j.dao.AbstractDao;
import sogeti.cir.fsn.model.Intervention;

public interface InterventionDAO extends AbstractDao<Intervention> {

}
