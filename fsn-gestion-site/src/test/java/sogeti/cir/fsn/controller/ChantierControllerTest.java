package sogeti.cir.fsn.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.JsonNode;

import sogeti.cir.fsn.FsnGestionSiteApplication;
import sogeti.cir.fsn.api.neo4j.service.AbstractService;
import sogeti.cir.fsn.api.neo4j.service.NotValidObjectException;
import sogeti.cir.fsn.controller.rest.ChantierRestController;
import sogeti.cir.fsn.model.Chantier;
import sogeti.cir.fsn.service.ChantierService;
import sogeti.cir.fsn.test.tools.AbstractTUController;
import sogeti.cir.fsn.test.tools.ControllerAction;
import sogeti.cir.fsn.test.tools.ITBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FsnGestionSiteApplication.class})
@AutoConfigureMockMvc
public class ChantierControllerTest extends AbstractTUController<Chantier>{

	@MockBean
	private ChantierService service;

	public ChantierControllerTest() {
		super(Chantier.NAME.toLowerCase()+"s");
	}

	
	@Test
	public void create_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Chantier createdChantier = new Chantier();
		createdChantier.setNom(data.get("nom").asText());
		createdChantier.setNomAbrege(data.get("nomAbrege").asText());
		createdChantier.setNumero(data.get("numero").asText());
		createdChantier.setNumeroINSEE(data.get("numeroINSEE").asText());
		createdChantier.setId(1L);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.CREATE)
				.withInputSchema(jrl.getJsonSchema(builder.getAction(), true))
				.withInput(data)
				.withValidInput()
				.withExpectedReturnedStatus(status().isCreated())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();
		
		Mockito.when(service.save(Mockito.any(Chantier.class))).thenReturn(createdChantier);
		
		this.testFromBuilder(builder);
		
	}
	
	
	@Test
	public void find_one_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Chantier consultedChantier = new Chantier();
		consultedChantier.setNom(data.get("nom").asText());
		consultedChantier.setNomAbrege(data.get("nomAbrege").asText());
		consultedChantier.setNumero(data.get("numero").asText());
		consultedChantier.setNumeroINSEE(data.get("numeroINSEE").asText());
		consultedChantier.setId(1L);
		
		ITBuilder builder = new ITBuilder();
		String[] args = {consultedChantier.getId()+""};
		builder.withAction(ControllerAction.DISPLAY)
				.withPathArguments(args)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput()
				.withPostAsserts( (inputArgs, inputBody, outputBody) -> {
					Assertions.assertThat(inputArgs[0])
						.isEqualTo(outputBody.get("id").asText());
				});
		
		Mockito.when(service.findOne(Mockito.eq(consultedChantier.getId()))).thenReturn(consultedChantier);

		this.testFromBuilder(builder);
	}
	
	
	@Test
	public void find_all_page_success() throws Exception {
		JsonNode data = this.jrl.getJsonRessouce(ControllerAction.CREATE, true);
		Chantier consultedChantier;
		ArrayList<Chantier> list = new ArrayList<Chantier>();
		for(int i = 0; i < 2; i++) {
			consultedChantier = new Chantier();
			consultedChantier.setNom(data.get("nom").asText());
			consultedChantier.setNomAbrege(data.get("nomAbrege").asText());
			consultedChantier.setNumero(data.get("numero").asText());
			consultedChantier.setNumeroINSEE(data.get("numeroINSEE").asText());
			consultedChantier.setId(i+1L);
			list.add(consultedChantier);
		}

		Pageable page = new PageRequest(0,20);
		Page<Chantier> returnedPage = new PageImpl<Chantier>(list);
		Mockito.when(service.findAllByPage(page)).thenReturn(returnedPage);
		
		ITBuilder builder = new ITBuilder();
		builder.withAction(ControllerAction.DISPLAY_ALL)
				.withExpectedReturnedStatus(status().isOk())
				.withOutputSchema(jrl.getJsonSchema(builder.getAction(), false))
				.withValidOutput();

		this.testFromBuilder(builder);
	}
	
	
}
